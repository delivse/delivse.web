﻿using AutoMapper;
using delivse.web.Controllers.UserController;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Xunit;

namespace delivse.web.XUnitTest
{
    public class CustomerManagementTest
    {
        public CustomerManagement Customer_Management()
        {
            return new CustomerManagement
            {
                Id=Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"), TripId=Guid.NewGuid(), CustomerId=Guid.NewGuid(), Time=DateTime.Now,DriverId=Guid.NewGuid(), Ratings=5, Comments="Awesome"
            };
        }

        [Fact]
        public async void GetCustomerByIdAsync_OK()
        {
            var mockCustomerRepo = new Mock<ICustomerManagement>();
            var mockTripRepo = new Mock<ITripManagement>();

            mockCustomerRepo.Setup(x => x.GetCustomerManagement(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"))).ReturnsAsync(Customer_Management());
            var controller = new CustomerManagementController(mockTripRepo.Object, mockCustomerRepo.Object);
            IActionResult actionResult = await controller.Get(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"));

            var objresult = Assert.IsType<OkObjectResult>(actionResult);
            var result = objresult.Value as CustomerManagementDTO;
            Assert.NotNull(actionResult);
            Assert.NotNull(objresult.Value);
            Assert.Equal(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"),result.Id);
            Assert.Equal(200, objresult.StatusCode);
        }

        [Fact]
        public async void GetCustomerByIdAsync_NotFound()
        {
            var mockCustomerRepo = new Mock<ICustomerManagement>();
            var mockTripRepo = new Mock<ITripManagement>();
            Guid Id = Guid.Empty;
            _ = Guid.TryParse("d8abaa22-pd28-4ea7-b7e4-4d7947bd03e1",out Id);
            mockCustomerRepo.Setup(x => x.GetCustomerManagement(Id)).ReturnsAsync((CustomerManagement)null);
            var controller = new CustomerManagementController(mockTripRepo.Object, mockCustomerRepo.Object);
            IActionResult actionResult = await controller.Get(Id);

            var objresult = Assert.IsType<NotFoundObjectResult>(actionResult);
            
            Assert.NotNull(actionResult);
            Assert.Equal(404, objresult.StatusCode);
        }

        [Fact]
        public async void PostCustomerManagementAsync_BadRequest()
        {
            var mockCustomerRepo = new Mock<ICustomerManagement>();
            var mockTripRepo = new Mock<ITripManagement>();

            var controller = new CustomerManagementController(mockTripRepo.Object, mockCustomerRepo.Object);
            controller.ModelState.AddModelError("Error", "Invalid State");
            IActionResult result = await controller.Post(null);

            Assert.NotNull(result);
            var badrequestObj = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badrequestObj.StatusCode);
        }

        [Fact]
        public async void PostCustomerManagementAsync_BadRequest_NoTrip()
        {
            var mockCustomerRepo = new Mock<ICustomerManagement>();
            var mockTripRepo = new Mock<ITripManagement>();

            var controller = new CustomerManagementController(mockTripRepo.Object, mockCustomerRepo.Object);
            var cstManagement = new CustomerManagementDTO
            {
                Id = Guid.NewGuid(),
                CustomerId = Guid.NewGuid(),
                TripId = Guid.NewGuid(),
                DriverId = Guid.NewGuid(),
                Ratings = 5,
                Time = DateTime.Now.AddSeconds(-20),
                Comments = "Good Service"
            };
            IActionResult result = await controller.Post(cstManagement);

            Assert.NotNull(result);
            var badobjrequest = Assert.IsType<BadRequestObjectResult>(result);
            Assert.NotNull(badobjrequest.Value);
            Assert.Equal(400, badobjrequest.StatusCode);
        }
        

        [Fact]
        public async void PutCustomerManagementAsync_NotFound()
        {
            var mockCustomerRepo = new Mock<ICustomerManagement>();
            var mockTripRepo = new Mock<ITripManagement>();
            var controller = new CustomerManagementController(mockTripRepo.Object, mockCustomerRepo.Object);

            IActionResult result = await controller.Put(Guid.NewGuid(), (CustomerManagementDTO)null);
            var NotFoundObject = Assert.IsType<NotFoundObjectResult>(result);

            Assert.NotNull(result);
            Assert.NotNull(NotFoundObject.Value);
            Assert.Equal(404, NotFoundObject.StatusCode);
        }

        [Fact]
        public async void PutCustomerManagementAsync_BadRequest()
        {
            var mockCustomerRepo = new Mock<ICustomerManagement>();
            var mockTripRepo = new Mock<ITripManagement>();

            var controller = new CustomerManagementController(mockTripRepo.Object, mockCustomerRepo.Object);
            controller.ModelState.AddModelError("Error", "Invalid State");
            IActionResult result = await controller.Put(Guid.NewGuid(),null);

            Assert.NotNull(result);
            var badRequestObj = Assert.IsType<BadRequestObjectResult>(result);
            Assert.Equal(400, badRequestObj.StatusCode);
        }

    }
}
