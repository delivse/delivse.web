﻿using delivse.web.Controllers.DriverController;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace delivse.web.XUnitTest
{
    public class DriverManagementTest
    {
        private Mock<IDriverManagement> mockdriverManagement;
        private Mock<IDriverPositionManagement> mockpositionManagement;
        private Mock<IUserStore<DelviseUser>> storeManager;


        [Fact]
        public async void PostActive_BadRequest()
        {

            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            this.mockdriverManagement = new Mock<IDriverManagement>();
            this.mockpositionManagement = new Mock<IDriverPositionManagement>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);

            var controller = new DriverManagementController(mockdriverManagement.Object, mockpositionManagement.Object, userManager);
            controller.ModelState.AddModelError("Error", "Entity is Null");
            IActionResult actionResult = await controller.Active((DriverManagementDTO)null);
            var BadObjResult = Assert.IsType<BadRequestObjectResult>(actionResult);

            Assert.NotNull(actionResult);
            Assert.NotNull(BadObjResult.Value);
            Assert.Equal(400, BadObjResult.StatusCode);
        }

        [Fact]
        public async void PostPosition_BadRequest()
        {
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            this.mockdriverManagement = new Mock<IDriverManagement>();
            this.mockpositionManagement = new Mock<IDriverPositionManagement>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);

            var controller = new DriverManagementController(mockdriverManagement.Object, mockpositionManagement.Object, userManager);
            controller.ModelState.AddModelError("Error", "Entity is Null");
            IActionResult actionResult = await controller.Position((DriverPositionDTO)null);
            var BadObjResult = Assert.IsType<BadRequestObjectResult>(actionResult);

            Assert.NotNull(actionResult);
            Assert.NotNull(BadObjResult.Value);
            Assert.Equal(400, BadObjResult.StatusCode);
        }
    }
}
