﻿using delivse.web.Controllers.UserController;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Models.Enum;
using delivse.web.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace delivse.web.XUnitTest
{
    public class CustomerTripManagementTest
    {
        public List<TripManagement> GetCustomerTrips()
        {
            return new List<TripManagement>
            {
                new TripManagement{Id=Guid.NewGuid(), CustomerId=Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"), DriverId=Guid.NewGuid(),ClientPhoneNumber="0727518227", FromTime=DateTime.Now.AddMinutes(-20), IsScheduled=false, IsCancelled=false, IsCompleted=true, IsHappening=false, ToTime=DateTime.Now.AddMinutes(-5), Description="File Delivery", TripToken="fgffibif", DestinationLatitude=63.051M, DestinationLongitude=8.21M, OriginLatitude=62.999M, OriginLongitude=7.02M, Time=Time.DayTime, Weather=Weather.Rainy },
                new TripManagement{Id=Guid.NewGuid(), CustomerId=Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"), DriverId=Guid.NewGuid(),ClientPhoneNumber="0727518227", FromTime=DateTime.Now.AddMinutes(-20), IsScheduled=false, IsCancelled=false, IsCompleted=true, IsHappening=false, ToTime=DateTime.Now.AddMinutes(-5), Description="File Delivery", TripToken="fgffibif", DestinationLatitude=63.011M, DestinationLongitude=8.30M, OriginLatitude=63.099M, OriginLongitude=8.02M, Time=Time.DayTime, Weather=Weather.Calm }
            };
        }

        public TripManagement GetCustomerTrip()
        {
            return new TripManagement { Id = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), CustomerId = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"), DriverId = Guid.NewGuid(), ClientPhoneNumber = "0727518227", FromTime = DateTime.Now.AddMinutes(-20), IsScheduled = false, IsCancelled = false, IsCompleted = true, IsHappening = false, ToTime = DateTime.Now.AddMinutes(-5), Description = "File Delivery", TripToken = "fgffibif", DestinationLatitude = 63.051M, DestinationLongitude = 8.21M, OriginLatitude = 62.999M, OriginLongitude = 7.02M, Time = Time.DayTime, Weather = Weather.Rainy };
        }

        public TripManagementDTO GetTripManagementDTO()
        {
            return new TripManagementDTO { Id = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), CustomerId = Guid.NewGuid(), DriverId = Guid.NewGuid(), ClientPhoneNumber = "0727518227", FromTime = DateTime.Now.AddMinutes(-20), IsScheduled = false, IsCancelled = false, IsCompleted = true, IsHappening = false, ToTime = DateTime.Now.AddMinutes(-5), Description = "File Delivery", TripToken = "fgffibif", DestinationLatitude = 63.051M, DestinationLongitude = 8.21M, OriginLatitude = 62.999M, OriginLongitude = 7.02M, Time = Time.DayTime, Weather = Weather.Rainy };
        }


        private Mock<ITripManagement> mockTrip;
        private Mock<ITripTracker> mockTripTracker;
        private Mock<IDriver> mockDriver;
        private Mock<IDriverPositionManagement> mockDriverPosition;
        private Mock<ICustomerManagement> mockCustomerManagement;
        private Mock<IUserStore<DelviseUser>> mockuserStore;

        [Fact]
        public async void GetCustomerTrips_Ok()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);

            mockTrip.Setup(x => x.GetTripsByUser(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"))).ReturnsAsync(GetCustomerTrips());
            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);

            IActionResult actionresult = await controller.GetTrips(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"));
            var objresult = Assert.IsType<OkObjectResult>(actionresult);
            var result = objresult.Value as List<TripManagementDTO>;

            Assert.NotNull(actionresult);
            Assert.NotNull(objresult.Value);
            Assert.Equal(result.Count, GetCustomerTrips().Count);
            Assert.Equal(200, objresult.StatusCode);
        }

        [Fact]
        public async void GetCustomerTrips_NoContent()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);


            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);

            IActionResult actionResult = await controller.GetTrips(null);
            var noContentObj = Assert.IsType<NoContentResult>(actionResult);

            Assert.NotNull(actionResult);
            Assert.Equal<int>(204, noContentObj.StatusCode);

        }

        [Fact]
        public async void GetCustomerTrips_NotFound()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);

            mockTrip.Setup(x => x.GetTripsByUser(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"))).ReturnsAsync((List<TripManagement>)null);
            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);

            IActionResult actionresult = await controller.GetTrips(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"));
            var objresult = Assert.IsType<NotFoundObjectResult>(actionresult);
            var result = objresult.Value as List<TripManagementDTO>;

            Assert.NotNull(actionresult);
            Assert.NotNull(objresult.Value);
            Assert.Equal(404, objresult.StatusCode);
        }

        [Fact]
        public async void GetCustomerTrip_Ok()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);

            mockTrip.Setup(x => x.GetTripByUser(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"))).ReturnsAsync(GetCustomerTrip());
            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);

            IActionResult actionresult = await controller.GetTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"));
            var objresult = Assert.IsType<OkObjectResult>(actionresult);
            var result = objresult.Value as TripManagementDTO;

            Assert.NotNull(actionresult);
            Assert.NotNull(objresult.Value);
            Assert.Equal(200, objresult.StatusCode);
        }

        [Fact]
        public async void GetCustomerTrip_NoContent()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);


            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);

            IActionResult actionResult = await controller.GetTrip(null);
            var noContentObj = Assert.IsType<NoContentResult>(actionResult);

            Assert.NotNull(actionResult);
            Assert.Equal(204, noContentObj.StatusCode);

        }

        [Fact]
        public async void GetCustomerTrip_NotFound()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);

            mockTrip.Setup(x => x.GetTripByUser(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"))).ReturnsAsync((TripManagement)null);
            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);

            IActionResult actionresult = await controller.GetTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"));
            var objresult = Assert.IsType<NotFoundObjectResult>(actionresult);
            var result = objresult.Value as TripManagementDTO;

            Assert.NotNull(actionresult);
            Assert.NotNull(objresult.Value);
            Assert.Equal(404, objresult.StatusCode);
        }

        [Fact]
        public async void PutStartTrip_BadRequest()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);
            mockTrip.Setup(x => x.GetTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"))).ReturnsAsync(GetCustomerTrip());

            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);
            controller.ModelState.AddModelError("Error", "Invalid State");
            IActionResult actionResult = await controller.StartTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), (TripManagementDTO)null);
            var objresult =Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.Equal(400, objresult.StatusCode);
        }

        [Fact]
        public async void PutStartTrip_NotFound()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);

            mockTrip.Setup(x => x.GetTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"))).ReturnsAsync((TripManagement)null);
            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);
            IActionResult actionResult = await controller.StartTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), GetTripManagementDTO());

            var objresult =Assert.IsType<NotFoundObjectResult>(actionResult);
            Assert.Equal(404, objresult.StatusCode);
        }

        [Fact]
        public async void PutCancelTrip_NotFound()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);

            mockTrip.Setup(x => x.GetTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"))).ReturnsAsync((TripManagement)null);
            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);
            IActionResult actionResult = await controller.CancelTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), GetTripManagementDTO());

            var objresult = Assert.IsType<NotFoundObjectResult>(actionResult);
            Assert.Equal(404, objresult.StatusCode);
        }

        [Fact]
        public async void PutCancelTrip_BadRequest()
        {
            mockTrip = new Mock<ITripManagement>();
            mockTripTracker = new Mock<ITripTracker>();
            mockDriver = new Mock<IDriver>();
            mockDriverPosition = new Mock<IDriverPositionManagement>();
            mockCustomerManagement = new Mock<ICustomerManagement>();
            mockuserStore = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(mockuserStore.Object, null, null, null, null, null, null, null, null);
            mockTrip.Setup(x => x.GetTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"))).ReturnsAsync(GetCustomerTrip());

            var controller = new CustomerTripManagementController(mockTrip.Object, mockTripTracker.Object
                                        , mockDriver.Object, mockDriverPosition.Object, mockCustomerManagement.Object, userManager);
            controller.ModelState.AddModelError("Error", "Wrong Entity Passed");
            IActionResult actionResult = await controller.CancelTrip(Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), (TripManagementDTO)null);
            var objresult = Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.Equal(400, objresult.StatusCode);
        }
    }
}
