﻿using delivse.web.Controllers.DriverController;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using delivse.web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace delivse.web.XUnitTest
{
    public class DriverTripManagementTest
    {
        private Mock<IUserStore<DelviseUser>> storeManager;
        private Mock<ITripManagement> tripManagement;
        private Mock<IDriverPositionManagement> driverPosition;
        private Mock<INearestDriver> nearestDriver;
        private Guid Id = Guid.Empty;

        public TripManagement GetDriverTrip()
        {
            return new TripManagement { Id = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), CustomerId = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"), DriverId = Guid.NewGuid(), ClientPhoneNumber = "0727518227", FromTime = DateTime.Now.AddMinutes(-20), IsScheduled = false, IsCancelled = false, IsCompleted = true, IsHappening = false, ToTime = DateTime.Now.AddMinutes(-5), Description = "File Delivery", TripToken = "TD8CD48", DestinationLatitude = 63.051M, DestinationLongitude = 8.21M, OriginLatitude = 62.999M, OriginLongitude = 7.02M, Time = Models.Enum.Time.DayTime, Weather = Models.Enum.Weather.Rainy };
        }

        public TripManagementDTO TripDTO()
        {
            return new TripManagementDTO { Id = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1"), CustomerId = Guid.Parse("d8abaa22-bd28-4ea7-b7e4-4d7947bd03e1"), DriverId = Guid.NewGuid(), ClientPhoneNumber = "0727518227", FromTime = DateTime.Now.AddMinutes(-20), IsScheduled = false, IsCancelled = false, IsCompleted = true, IsHappening = false, ToTime = DateTime.Now.AddMinutes(-5), Description = "File Delivery", TripToken = "HBC54D", DestinationLatitude = 63.051M, DestinationLongitude = 8.21M, OriginLatitude = 62.999M, OriginLongitude = 7.02M, Time = Models.Enum.Time.DayTime, Weather = Models.Enum.Weather.Rainy };

        }

        [Fact]
        public async void GetTrip_NotFound()
        {
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            this.tripManagement.Setup(x => x.GetUnassignedTrip()).ReturnsAsync((TripManagement)null);
            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);
            IActionResult actionResult = await controller.GetTrip();

            var NotfoundObj = Assert.IsType<NotFoundObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(NotfoundObj.Value);
            Assert.Equal(404, NotfoundObj.StatusCode);
        }

        [Fact]
        public async void PutDriverAccept_BadRequest()
        {
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);
            controller.ModelState.AddModelError("Error", "Invalid State");
            IActionResult actionResult = await controller.DriverAccept(Guid.NewGuid(), (TripManagementDTO)null);

            var NotfoundObj = Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(NotfoundObj.Value);
            Assert.Equal(400, NotfoundObj.StatusCode);

        }

        [Fact]
        public async void PutDriverAccept_NotFound()
        {
            _ = Guid.TryParse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1", out this.Id);
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            tripManagement.Setup(x => x.GetTrip(this.Id)).ReturnsAsync((TripManagement)null);
            
            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);
            IActionResult actionResult = await controller.DriverAccept(Id, (TripManagementDTO)null);
            Id = Guid.Empty;
            var NotFoundObj = Assert.IsType<NotFoundObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(NotFoundObj.Value);
            Assert.Equal(404, NotFoundObj.StatusCode);
        }

        [Fact]
        public async void PutDriverAccept_BadRequest_IsNotHappening()
        {
            _ = Guid.TryParse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1", out this.Id);
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            tripManagement.Setup(x => x.GetTrip(this.Id)).ReturnsAsync(GetDriverTrip());

            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);
            IActionResult actionResult = await controller.DriverAccept(Id, TripDTO());
            Id = Guid.Empty;
            var NotHappeningBadRequest = Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(NotHappeningBadRequest.Value);
            Assert.Equal(400, NotHappeningBadRequest.StatusCode);
        }

        [Fact]
        public async void PutCompletedTrip_NotFound()
        {
            _ = Guid.TryParse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1", out this.Id);
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            tripManagement.Setup(x => x.GetHappeningTrip(this.Id)).ReturnsAsync((TripManagement)null);
            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);

            IActionResult actionResult = await controller.CompleteTrip(Id, string.Empty);
            Id = Guid.Empty;
            var NotFoundObj = Assert.IsType<NotFoundObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(NotFoundObj.Value);
            Assert.Equal(404, NotFoundObj.StatusCode);
        }

        [Fact]
        public async void PutCompletedTrip_BadRequest()
        {
            _ = Guid.TryParse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1", out this.Id);
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            tripManagement.Setup(x => x.GetHappeningTrip(this.Id)).ReturnsAsync(GetDriverTrip());
            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);

            IActionResult actionResult = await controller.CompleteTrip(Id, "H5F51VD");
            Id = Guid.Empty;
            var BadRequestObj = Assert.IsType<BadRequestObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(BadRequestObj.Value);
            Assert.Equal(400, BadRequestObj.StatusCode);
        }

        [Fact]
        public async void PutCompletedTrip_Ok()
        {
            _ = Guid.TryParse("d8abaa22-bd28-4ea7-b7e4-4d7947bd63e1", out this.Id);
            this.tripManagement = new Mock<ITripManagement>();
            this.driverPosition = new Mock<IDriverPositionManagement>();
            this.nearestDriver = new Mock<INearestDriver>();
            this.storeManager = new Mock<IUserStore<DelviseUser>>();
            var userManager = new UserManager<DelviseUser>(storeManager.Object, null, null, null, null, null, null, null, null);
            tripManagement.Setup(x => x.GetHappeningTrip(this.Id)).ReturnsAsync(GetDriverTrip());
            var controller = new DriverTripManagementController(userManager, tripManagement.Object, driverPosition.Object, nearestDriver.Object);

            IActionResult actionResult = await controller.CompleteTrip(Id, "TD8CD48");
            Id = Guid.Empty;
            var okObjResult = Assert.IsType<OkObjectResult>(actionResult);
            Assert.NotNull(actionResult);
            Assert.NotNull(okObjResult.Value);
            Assert.Equal(200,okObjResult.StatusCode);
        }
    }
}
