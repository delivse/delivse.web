﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using delivse.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.DAL
{
    public class DelviseContext:IdentityDbContext<DelviseUser,DelviseRole, Guid>
    {
        public DelviseContext(DbContextOptions<DelviseContext> options)
            :base(options)
        {

        }

        public virtual DbSet<CustomerManagement> Customermanagement { get; set; }
        public virtual DbSet<DriverManagement> Drivermanagement { get; set; }
        public virtual DbSet<DriverPositionManagement> DriverPM { get; set; }
        public virtual DbSet<TripManagement> Tripmanagement { get; set; }
        public virtual DbSet<TripTracker> TripTracker { get; set; }
        public virtual DbSet<CardBilling> Cardbilling { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }
        public virtual DbSet<GeneratedTripKey> GeneratedTripKey { get; set; }
        public virtual DbSet<Driver> Driver { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }


        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TripManagement>().HasOne(x => x.Driver).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<TripManagement>().HasOne(x => x.Customer).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Payment>().HasOne(x => x.Driver).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Payment>().HasOne(x => x.Customer).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<CustomerManagement>().HasOne(x => x.Driver).WithOne().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<CustomerManagement>().HasOne(x => x.Customer).WithOne().OnDelete(DeleteBehavior.Cascade);

            builder.Entity<GeneratedTripKey>(c =>
            {
                c.HasIndex(x => x.GeneratedCode).IsUnique();
            });

            builder.Entity<Driver>(c =>
            {
                c.HasIndex(x => x.GeneratedID).IsUnique();
            });

            builder.Entity<IdentityUser>(c =>
            {
                c.ToTable("QuanUser");
                c.HasKey(x => x.Id);
            });

            builder.Entity<IdentityRole>(c =>
            {
                c.ToTable("Roles");
                c.HasKey(x => x.Id);
            });

            builder.Entity<IdentityUserRole<string>>(c => {
                c.ToTable("UserRoles");
                c.HasKey(x => new { x.UserId, x.RoleId });
            });

            builder.Entity<IdentityUserClaim<string>>(c =>
            {
                c.ToTable("UserClaim");
                c.HasKey(x => new { x.Id, x.UserId });
            });

            builder.Entity<IdentityUserLogin<string>>(c =>
            {
                c.ToTable("UserLogin");
                c.HasKey(x => new { x.ProviderKey, x.UserId });
            });

            builder.Entity<IdentityRoleClaim<string>>(c =>
            {
                c.ToTable("RoleClaims");
                c.HasKey(x => new { x.Id, x.RoleId });
            });

            builder.Entity<IdentityUserToken<string>>(c =>
            {
                c.ToTable("UserTokens");
                c.HasKey(x => x.UserId);
            });

            builder.Entity<DelviseUser>(c =>
            {
                c.ToTable("QuanUser");
            });

            builder.Entity<DelviseRole>(c =>
            {
                c.ToTable("Roles");
            });

            builder.Entity<CardBilling>(c =>
            {
                c.HasIndex(x => x.CardNumber);
            });
        }
    }
}
