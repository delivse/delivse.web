﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;

namespace delivse.web.Repositories
{
    public class TripTrackerRepo : Repository<TripTracker>,ITripTracker
    {
        private DelviseContext DelviseContext
        {
            get { return context as DelviseContext; }
        }

        public TripTrackerRepo(DelviseContext context)
            :base(context)
        {
        }
        

        public async Task<IEnumerable<TripTracker>> GetDriverDayTrips(Guid Id)
        {
            var trips = await this.DelviseContext.Tripmanagement.Where(x => x.DriverId == Id && x.ToTime == DateTime.Today).Select(i => i.Id).ToListAsync();
            List<TripTracker> tripslist = new List<TripTracker>();


            foreach (var trip in trips)
            {
                var trackedtrip = await this.DelviseContext.TripTracker.Where(t => t.TripManagementId == trip).FirstOrDefaultAsync();
                tripslist.Add(trackedtrip);
            }

            return tripslist;
        }
        
        
    }
}
