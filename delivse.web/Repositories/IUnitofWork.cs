﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface IUnitofWork : IDisposable
    {
        ICreditCard CreditCard { get; }
        ICustomer Customer { get; }
        ICustomerManagement CustomerManagement { get; }
        IDriver Driver { get; }
        IDriverManagement DriverManagement { get; }
        IDriverPositionManagement DriverPositionManagement { get; }
        IGeneratedTripKey GeneratedTripKey { get; }
        IPayment Payment { get; }
        ITripManagement TripManagement { get; }
        ITripTracker TripTracker { get; }
        Task Save();
    }
}
