﻿using delivse.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface ITripManagement: IRepository<TripManagement>
    {
        Task<TripManagement> GetHappeningTrip(Guid Id);
        Task<TripManagement> GetTripByDriver(Guid Id);
        Task<TripManagement> GetTripByUser(Guid Id);
        Task<TripManagement> GetScheduledTripByUser(Guid Id);
        Task<TripManagement> GetScheduledTripByDriver(Guid Id);
        Task<TripManagement> GetUnassignedTrip();
        Task<IEnumerable<TripManagement>> GetCompletedTripsByUser(Guid Id);
        Task<IEnumerable<TripManagement>> GetCompletedTripsByDriver(Guid Id);
        Task<IEnumerable<TripManagement>> GetTripsByDriver(Guid Id);
        Task<IEnumerable<TripManagement>> GetTripsByUser(Guid Id);
        Task<IEnumerable<TripManagement>> GetScheduledTrips();
        Task<IEnumerable<TripManagement>> GetScheduledTripsByUser(Guid Id);
        Task<IEnumerable<TripManagement>> GetScheduledTripsByDriver(Guid Id);
    }
}
