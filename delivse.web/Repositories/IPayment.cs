﻿using delivse.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface IPayment: IRepository<Payment>
    {

        Task<Payment> GetPaymentUser(Guid Id);
        Task<Payment> GetPaymentDriver(Guid Id);
        Task<IDictionary<DateTime, decimal?>> GetTotalDayPaymentDriver(Guid Id);
        Task<IDictionary<DateTime, List<Payment>>> GetPaymentsDriver(Guid Id);
        Task<IDictionary<DateTime, List<Payment>>> GetPaymentsUser(Guid Id);
    }
}
