﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;

namespace delivse.web.Repositories
{
    public class TripManagementRepo : Repository<TripManagement>,ITripManagement
    {
        private DelviseContext DelviseContext
        {
            get { return context as DelviseContext; }
        }
        public TripManagementRepo(DelviseContext context)
            :base(context)
        {
        }

        public async Task<TripManagement> GetCompletedTripByUser(Guid Id)
        {
            var completed = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id && x.IsCompleted == true).FirstOrDefaultAsync();
            return completed;
        }

        public async Task<IEnumerable<TripManagement>> GetCompletedTripsByDriver(Guid Id)
        {
            var completedbyDriver = await this.DelviseContext.Tripmanagement.Where(x => x.DriverId == Id && x.IsCompleted == true).ToListAsync();
            return completedbyDriver;
        }

        public async Task<IEnumerable<TripManagement>> GetCompletedTripsByUser(Guid Id)
        {
            var completedbyUser = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id && x.IsCompleted == true).ToListAsync();
            return completedbyUser;
        }

        public async Task<TripManagement> GetHappeningTrip(Guid Id)
        {
            var trip = await this.DelviseContext.Tripmanagement.Where(x => x.IsHappening == true && x.Id == Id).FirstOrDefaultAsync();
            return trip;
        }

        public async Task<TripManagement> GetInCompletedTripByUser(Guid Id)
        {
            var incompletedtrip = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id && x.IsCompleted == false).FirstOrDefaultAsync();
            return incompletedtrip;
        }

        public async Task<IEnumerable<TripManagement>> GetInCompletedTrips()
        {
            var incompletetrips = await this.DelviseContext.Tripmanagement.Where(x => x.IsCompleted == false).ToListAsync();
            return incompletetrips;
        }

        public async Task<TripManagement> GetScheduledTripByDriver(Guid Id)
        {
            var scheduledtripbyDriver = await this.DelviseContext.Tripmanagement.Where(x => x.IsScheduled == true && x.ScheduledTime == DateTime.Today && x.IsCompleted == false && x.DriverId == Id).FirstOrDefaultAsync();
            return scheduledtripbyDriver;
        }

        public async Task<TripManagement> GetScheduledTripByUser(Guid Id)
        {
            var scheduledtripbyUser = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id && x.IsScheduled == true && x.ScheduledTime == DateTime.Today && x.IsCompleted == false).FirstOrDefaultAsync();
            return scheduledtripbyUser;
        }

        public async Task<IEnumerable<TripManagement>> GetScheduledTrips()
        {
            var scheduledtrips = await this.DelviseContext.Tripmanagement.Where(x => x.IsScheduled == true && x.ScheduledTime == DateTime.Today).ToListAsync();
            return scheduledtrips;
        }

        public async Task<IEnumerable<TripManagement>> GetScheduledTripsByDriver(Guid Id)
        {
            var scheduledtripsbyDriver = await this.DelviseContext.Tripmanagement.Where(x => x.IsScheduled == true && x.ScheduledTime >= DateTime.Today && x.IsCompleted == false && x.DriverId == Id).ToListAsync();
            return scheduledtripsbyDriver;
        }

        public async Task<IEnumerable<TripManagement>> GetScheduledTripsByUser(Guid Id)
        {
            var scheduledtripsbyUser = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id && x.IsScheduled == true && x.ScheduledTime >= DateTime.Today && x.IsCompleted == false).ToListAsync();
            return scheduledtripsbyUser;
        }
        

        public async Task<TripManagement> GetTripByDriver(Guid Id)
        { 
            var trip = await this.DelviseContext
                .Tripmanagement.Where(i => i.DriverId == Id).FirstOrDefaultAsync();
            return trip;
        }

        //Returns a User trip which occured less than 10mins ago
        public async Task<TripManagement> GetTripByUser(Guid Id)
        {
            // most recent
            var time = DateTime.Now.AddMinutes(-10);
            var tripbyUser = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id && x.FromTime >= time && x.IsCancelled == false && x.IsScheduled == false ).FirstOrDefaultAsync();
            return tripbyUser;
        }

        public async Task<IEnumerable<TripManagement>> GetTripsByDriver(Guid Id)
        {
            var tripsmade = await this.DelviseContext.Tripmanagement.Where(x => x.DriverId == Id && x.FromTime == DateTime.Today).ToListAsync();
            return tripsmade;
        }

        public async Task<IEnumerable<TripManagement>> GetTripsByUser(Guid Id)
        {
            var trips = await this.DelviseContext.Tripmanagement.Where(x => x.CustomerId == Id).OrderByDescending(z => z.FromTime).ToListAsync();
            return trips;
        }

        public async Task<TripManagement> GetUnassignedTrip()
        {
            var unassigned = await this.DelviseContext.Tripmanagement.Where(x => x.DriverId == Guid.Empty && x.IsCancelled == false)
                                .Select(i => i).SingleOrDefaultAsync();
            return unassigned;
        }
        
    }
}
