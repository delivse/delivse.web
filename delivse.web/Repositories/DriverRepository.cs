﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;

namespace delivse.web.Repositories
{
    public class DriverRepository : Repository<Driver>,IDriver
    {
        private DelviseContext DelviseContext
        {
            get { return context as DelviseContext; }
        }
        public DriverRepository(DelviseContext context)
            :base(context)
        {
        }
        
    }
}
