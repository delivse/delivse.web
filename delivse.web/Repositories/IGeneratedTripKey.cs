﻿using delivse.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface IGeneratedTripKey: IRepository<GeneratedTripKey>
    {

    }
}
