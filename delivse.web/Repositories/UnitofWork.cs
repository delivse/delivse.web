﻿using delivse.web.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public class UnitofWork : IUnitofWork
    {
        protected readonly DelviseContext delviseContext;
        
        public ICreditCard CreditCard { get; private set; }
        public ICustomer Customer { get; private set; }
        public ICustomerManagement CustomerManagement { get; private set; }
        public IDriver Driver { get; private set; }
        public IDriverManagement DriverManagement { get; private set; }
        public IDriverPositionManagement DriverPositionManagement { get; private set; }
        public IGeneratedTripKey GeneratedTripKey { get; private set; }
        public IPayment Payment { get; private set; }
        public ITripManagement TripManagement { get; private set; }
        public ITripTracker TripTracker { get; private set; }

        public UnitofWork(DelviseContext context)
        {
            this.delviseContext = context;
            CreditCard = new CreditCardRepo(delviseContext);
            Customer = new CustomerRepository(delviseContext);
            CustomerManagement = new CustomerManagementRepo(delviseContext);
            Driver = new DriverRepository(delviseContext);
            DriverManagement = new DriverManagementRepo(delviseContext);
            GeneratedTripKey = new GeneratedKeyRepository(delviseContext);
            Payment = new PaymentRepo(delviseContext);
            TripManagement = new TripManagementRepo(delviseContext);
            TripTracker = new TripTrackerRepo(delviseContext);
        }

        public async Task Save()
        {
            await this.delviseContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            this.delviseContext.Dispose();
        }
    }
}
