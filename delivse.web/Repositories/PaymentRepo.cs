﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;

namespace delivse.web.Repositories
{
    public class PaymentRepo : Repository<Payment>,IPayment
    {
        private DelviseContext DelviseContext
        {
            get { return context as DelviseContext; }
        }
        public PaymentRepo(DelviseContext context)
            :base(context)
        {
        }

        
        public async Task<Payment> GetPaymentUser(Guid Id)
        {
            var payment = await this.DelviseContext.Payment.Where(x => x.CustomerId == Id).FirstOrDefaultAsync();
            return payment;
        }

        public async Task<Payment> GetPaymentDriver(Guid Id)
        {
            var payment = await this.DelviseContext.Payment.Where(x => x.TripTracker.TripManagement.DriverId == Id).FirstOrDefaultAsync();
            return payment;
        }
        

        
        public async Task<IDictionary<DateTime, decimal?>> GetTotalDayPaymentDriver(Guid Id)
        {
            var daypayment = await this.DelviseContext.Payment.Where(x => x.DriverId == Id && x.IsPaid == true)
                            .GroupBy(x => x.Date).ToDictionaryAsync(x => x.Key, pay => pay.Select(u => u.Amount).Sum());
            return daypayment;
        }

        public async Task<IDictionary<DateTime, List<Payment>>> GetPaymentsDriver(Guid Id)
        {
            var paymentstoDriver = await this.DelviseContext.Payment.Where(x => x.DriverId == Id && x.IsPaid == true)
                             .GroupBy(x => x.Date).ToDictionaryAsync(x => x.Key, l => l.Select(i => i).ToList());
            return paymentstoDriver;
        }

        public async Task<IDictionary<DateTime, List<Payment>>> GetPaymentsUser(Guid Id)
        {
            var paymentstoUser = await this.DelviseContext.Payment.Where(x => x.CustomerId == Id && x.IsPaid == true)
                            .GroupBy(x => x.Date).ToDictionaryAsync(x => x.Key, l => l.Select(i => i).ToList());

            return paymentstoUser;
        }
    }
}
