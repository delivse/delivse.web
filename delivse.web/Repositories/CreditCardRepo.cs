﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;

namespace delivse.web.Repositories
{
    public class CreditCardRepo : Repository<CardBilling>, ICreditCard
    {
        private DelviseContext Context
        {
            get { return context as DelviseContext; }
        }
        public CreditCardRepo(DelviseContext context)
            :base(context)
        {
        }
        
    }
}
