﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;
using delivse.web.Models.DTO;

namespace delivse.web.Repositories
{
    public class CustomerManagementRepo : Repository<CustomerManagement>,ICustomerManagement
    {
        private DelviseContext QuanContext {
            get { return context as DelviseContext; }
        }
        public CustomerManagementRepo(DelviseContext context)
            :base(context)
        {
        }
        
        
        public async Task<DriverAverageRatingDTO> GetDriverAverageRating(Guid Id)
        {
            var driverRatings = this.QuanContext.Customermanagement.Where(x => x.DriverId == Id).ToList()
                .Average(x => x.Ratings);
            var driverdetail = await this.QuanContext.Customermanagement.Where(x => x.DriverId == Id).Select(i => new DriverAverageRatingDTO
            {
                DriverUID = i.DriverId.Value,
                AverageRatings = driverRatings,
                FullName = i.Driver.FullName,
                Vehicle = i.Driver.VehicleDescription
            }).SingleOrDefaultAsync();

            return driverdetail;
        }
       
    }
}
