﻿using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;
using delivse.web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    /// <summary>
    /// Repository access layer for the Database
    /// </summary>
    public class DriverPositionManagementRepo : Repository<DriverPositionManagement>,IDriverPositionManagement
    {
        private DelviseContext DelviseContext
        {
            get { return context as DelviseContext; }
        }
        public DriverPositionManagementRepo(DelviseContext context)
            :base(context)
        {

        }

        
        public async ValueTask<PositionDTO> GetDriverCurrentPosition(Guid Id)
        {
            var lowerboundTime = DateTime.Now.AddSeconds(-2);
            var upperboundTime = DateTime.Now.AddMilliseconds(-200);
            //get current position
            var driverpos = await this.DelviseContext.DriverPM.Where(i => i.Time <= upperboundTime && i.Time>= lowerboundTime).Select(x => new PositionDTO
                                                    {
                                                        Longitude = x.Longitude,
                                                        Latitude = x.Latitude
                                                    }).SingleOrDefaultAsync();
            return driverpos;
        }

        public async ValueTask<IEnumerable<DriverPositionManagement>> GetDriverPosition()
        {
            DateTime time = DateTime.Now.AddMinutes(-3);
            var list = await this.DelviseContext.DriverPM.Where(c => c.Time >= time && c.IsDelivering == false).ToListAsync();

            return list;
        }

        public async ValueTask<DriverPositionManagement> GetRecentPosition(Guid Id)
        {
            var lowerboundTime = DateTime.Now.AddSeconds(-8);
            var upperboundTime = DateTime.Now.AddMilliseconds(-200);
            var recentPosition = await this.DelviseContext.DriverPM.Where(x => x.DriverId == Id && x.Time >= lowerboundTime && x.Time <= upperboundTime && x.IsDelivering == true).SingleOrDefaultAsync();
            return recentPosition;
        }
        
        public async Task<DriverPositionManagement> GetStoppedPosition(Guid Id)
        {
            throw new NotImplementedException();
        }

        public async ValueTask<IEnumerable<DriverPositionManagement>> GetUnassignedDrivers()
        {
            var unassignedDriver = await this.DelviseContext.DriverPM.Where(x => x.IsDelivering == false && x.IsStopped == false).Select(i => i).ToListAsync();
            return unassignedDriver;
        }

        
    }
}
