﻿using delivse.web.Models;
using delivse.web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface IDriverManagement:IRepository<DriverManagement>
    {

    }
}
