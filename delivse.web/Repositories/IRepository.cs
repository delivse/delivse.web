﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface IRepository<T>
    {
        Task<T> Get(Guid Id);
        Task<IEnumerable<T>> GetAll();
        Task Add(T entity);
        Task<T> Find(Guid Id);
        //Task Update(T entity);
        void Remove(T entity);
    }
}
