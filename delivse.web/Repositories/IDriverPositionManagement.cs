﻿using delivse.web.Models;
using delivse.web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface IDriverPositionManagement: IRepository<DriverPositionManagement>
    {
        ValueTask<IEnumerable<DriverPositionManagement>> GetDriverPosition();
        ValueTask<DriverPositionManagement> GetRecentPosition(Guid Id);
        Task<DriverPositionManagement> GetStoppedPosition(Guid Id);
        ValueTask<PositionDTO> GetDriverCurrentPosition(Guid Id);
        ValueTask<IEnumerable<DriverPositionManagement>> GetUnassignedDrivers();
    }
}
