﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using delivse.web.DAL;
using delivse.web.Models;

namespace delivse.web.Repositories
{
    public class CustomerRepository : Repository<Customer>,ICustomer
    {
        private DelviseContext DelviseContext
        {
            get { return context as DelviseContext; }
        }

        public CustomerRepository(DelviseContext context)
            :base(context)
        {

        }

        
    }
}
