﻿using delivse.web.DAL;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public class Repository<T> : IRepository<T> where T :class {

        protected readonly DelviseContext context;

        public Repository(DelviseContext context)
        {
            this.context = context;
        }

        public async Task Add(T entity)
        {
            await this.context.Set<T>().AddAsync(entity);
        }

        public async Task<T> Find(Guid Id)
        {
            return await this.context.Set<T>().FindAsync(Id);
        }

        public async Task<T> Get(Guid Id)
        {
            return await this.context.Set<T>().FindAsync(Id);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await this.context.Set<T>().ToListAsync();
        }

        public void Remove(T entity)
        {
             this.context.Set<T>().Remove(entity);
        }
        
        
    }
}
