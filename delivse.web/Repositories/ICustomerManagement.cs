﻿using delivse.web.Models;
using delivse.web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Repositories
{
    public interface ICustomerManagement: IRepository<CustomerManagement>
    {
        Task<DriverAverageRatingDTO> GetDriverAverageRating(Guid Id);
        //Task<CustomerManagement> GetCustomerManagement(Guid Id);
        //Task Update(CustomerManagement userManagement);
    }
}
