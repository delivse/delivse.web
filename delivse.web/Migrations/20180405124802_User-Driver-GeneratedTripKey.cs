﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace delivse.web.Migrations
{
    public partial class UserDriverGeneratedTripKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cardbilling_User_UserId",
                table: "Cardbilling");

            migrationBuilder.DropForeignKey(
                name: "FK_Drivermanagement_User_DriverId",
                table: "Drivermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_DriverPM_User_DriverId",
                table: "DriverPM");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_User_DriverId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_User_UserId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_User_DriverId",
                table: "Tripmanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_UserClaim_User_UserId",
                table: "UserClaim");

            migrationBuilder.DropForeignKey(
                name: "FK_UserLogin_User_UserId",
                table: "UserLogin");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_DriverId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_Tripmanagement_TripId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_UserId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_User_UserId",
                table: "UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTokens_User_UserId",
                table: "UserTokens");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_DriverId",
                table: "Usermanagement");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_UserId",
                table: "Usermanagement");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_PhoneNumber_Email_UserName",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_DriverId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Payment_DriverId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Payment_UserId",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "User");

            migrationBuilder.DropColumn(
                name: "City",
                table: "User");

            migrationBuilder.DropColumn(
                name: "ConfirmPassword",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "User");

            migrationBuilder.DropColumn(
                name: "IDNumber",
                table: "User");

            migrationBuilder.DropColumn(
                name: "LogbookNumber",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "User");

            migrationBuilder.DropColumn(
                name: "VehicleDescription",
                table: "User");

            migrationBuilder.DropColumn(
                name: "IsPaid",
                table: "TripTracker");

            migrationBuilder.RenameTable(
                name: "User",
                newName: "QuanUser");

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Usermanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "TripId",
                table: "Usermanagement",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<short>(
                name: "Ratings",
                table: "Usermanagement",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "Usermanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserId1",
                table: "Usermanagement",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "QuanUser",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginLongitude",
                table: "Tripmanagement",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginLatitude",
                table: "Tripmanagement",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DestinationLongitude",
                table: "Tripmanagement",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DestinationLatitude",
                table: "Tripmanagement",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Payment",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "Payment",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPaid",
                table: "Payment",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "TripId",
                table: "Payment",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "UserId1",
                table: "Payment",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Longitude",
                table: "DriverPM",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Latitude",
                table: "DriverPM",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "DriverPM",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "DriverId",
                table: "Drivermanagement",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Cardbilling",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<byte[]>(
                name: "CVV",
                table: "Cardbilling",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuanUser",
                table: "QuanUser",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Driver",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    DriverId = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Gender = table.Column<int>(nullable: false),
                    GeneratedID = table.Column<string>(maxLength: 8, nullable: true),
                    IDNumber = table.Column<string>(nullable: true),
                    LogbookNumber = table.Column<string>(nullable: true),
                    Ratings = table.Column<short>(nullable: false),
                    TaxPayerPIN = table.Column<string>(nullable: true),
                    VehicleDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Driver", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Driver_QuanUser_DriverId",
                        column: x => x.DriverId,
                        principalTable: "QuanUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_QuanUser_UserId",
                        column: x => x.UserId,
                        principalTable: "QuanUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeneratedTripKey",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ClientPhoneNumber = table.Column<string>(nullable: true),
                    DriverId = table.Column<Guid>(nullable: true),
                    GeneratedCode = table.Column<string>(maxLength: 10, nullable: true),
                    TripId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneratedTripKey", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeneratedTripKey_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GeneratedTripKey_Tripmanagement_TripId",
                        column: x => x.TripId,
                        principalTable: "Tripmanagement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_DriverId",
                table: "Usermanagement",
                column: "DriverId",
                unique: true,
                filter: "[DriverId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_UserId",
                table: "Usermanagement",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_UserId1",
                table: "Usermanagement",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_DriverId",
                table: "Tripmanagement",
                column: "DriverId",
                unique: true,
                filter: "[DriverId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_DriverId",
                table: "Payment",
                column: "DriverId",
                unique: true,
                filter: "[DriverId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_TripId",
                table: "Payment",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_UserId",
                table: "Payment",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_UserId1",
                table: "Payment",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_DriverId",
                table: "Driver",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_GeneratedID",
                table: "Driver",
                column: "GeneratedID",
                unique: true,
                filter: "[GeneratedID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_GeneratedTripKey_DriverId",
                table: "GeneratedTripKey",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneratedTripKey_GeneratedCode",
                table: "GeneratedTripKey",
                column: "GeneratedCode",
                unique: true,
                filter: "[GeneratedCode] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_GeneratedTripKey_TripId",
                table: "GeneratedTripKey",
                column: "TripId");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserId",
                table: "User",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cardbilling_User_UserId",
                table: "Cardbilling",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Drivermanagement_Driver_DriverId",
                table: "Drivermanagement",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DriverPM_Driver_DriverId",
                table: "DriverPM",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_Driver_DriverId",
                table: "Payment",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_TripTracker_TripId",
                table: "Payment",
                column: "TripId",
                principalTable: "TripTracker",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_User_UserId",
                table: "Payment",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_User_UserId1",
                table: "Payment",
                column: "UserId1",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_Driver_DriverId",
                table: "Tripmanagement",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserClaim_QuanUser_UserId",
                table: "UserClaim",
                column: "UserId",
                principalTable: "QuanUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserLogin_QuanUser_UserId",
                table: "UserLogin",
                column: "UserId",
                principalTable: "QuanUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_Driver_DriverId",
                table: "Usermanagement",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_Tripmanagement_TripId",
                table: "Usermanagement",
                column: "TripId",
                principalTable: "Tripmanagement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_UserId",
                table: "Usermanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_UserId1",
                table: "Usermanagement",
                column: "UserId1",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_QuanUser_UserId",
                table: "UserRoles",
                column: "UserId",
                principalTable: "QuanUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTokens_QuanUser_UserId",
                table: "UserTokens",
                column: "UserId",
                principalTable: "QuanUser",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cardbilling_User_UserId",
                table: "Cardbilling");

            migrationBuilder.DropForeignKey(
                name: "FK_Drivermanagement_Driver_DriverId",
                table: "Drivermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_DriverPM_Driver_DriverId",
                table: "DriverPM");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_Driver_DriverId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_TripTracker_TripId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_User_UserId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_User_UserId1",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_Driver_DriverId",
                table: "Tripmanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_UserClaim_QuanUser_UserId",
                table: "UserClaim");

            migrationBuilder.DropForeignKey(
                name: "FK_UserLogin_QuanUser_UserId",
                table: "UserLogin");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_Driver_DriverId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_Tripmanagement_TripId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_UserId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_UserId1",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRoles_QuanUser_UserId",
                table: "UserRoles");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTokens_QuanUser_UserId",
                table: "UserTokens");

            migrationBuilder.DropTable(
                name: "GeneratedTripKey");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_DriverId",
                table: "Usermanagement");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_UserId",
                table: "Usermanagement");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_UserId1",
                table: "Usermanagement");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_DriverId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuanUser",
                table: "QuanUser");

            migrationBuilder.DropIndex(
                name: "IX_Payment_DriverId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Payment_TripId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Payment_UserId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Payment_UserId1",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Usermanagement");

            migrationBuilder.DropColumn(
                name: "IsPaid",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "TripId",
                table: "Payment");

            migrationBuilder.DropColumn(
                name: "UserId1",
                table: "Payment");

            migrationBuilder.RenameTable(
                name: "QuanUser",
                newName: "User");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Usermanagement",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "TripId",
                table: "Usermanagement",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Ratings",
                table: "Usermanagement",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "Usermanagement",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPaid",
                table: "TripTracker",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginLongitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginLatitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DestinationLongitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "DestinationLatitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "User",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ConfirmPassword",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IDNumber",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LogbookNumber",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VehicleDescription",
                table: "User",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Payment",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "Payment",
                nullable: true,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Longitude",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Latitude",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "Drivermanagement",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Cardbilling",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AlterColumn<short>(
                name: "CVV",
                table: "Cardbilling",
                nullable: false,
                oldClrType: typeof(byte[]),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_User",
                table: "User",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_DriverId",
                table: "Usermanagement",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_UserId",
                table: "Usermanagement",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_DriverId",
                table: "Tripmanagement",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_User_PhoneNumber_Email_UserName",
                table: "User",
                columns: new[] { "PhoneNumber", "Email", "UserName" });

            migrationBuilder.CreateIndex(
                name: "IX_Payment_DriverId",
                table: "Payment",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_UserId",
                table: "Payment",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cardbilling_User_UserId",
                table: "Cardbilling",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Drivermanagement_User_DriverId",
                table: "Drivermanagement",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DriverPM_User_DriverId",
                table: "DriverPM",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_User_DriverId",
                table: "Payment",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_User_UserId",
                table: "Payment",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_User_DriverId",
                table: "Tripmanagement",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserClaim_User_UserId",
                table: "UserClaim",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserLogin_User_UserId",
                table: "UserLogin",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_DriverId",
                table: "Usermanagement",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_Tripmanagement_TripId",
                table: "Usermanagement",
                column: "TripId",
                principalTable: "Tripmanagement",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_UserId",
                table: "Usermanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRoles_User_UserId",
                table: "UserRoles",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTokens_User_UserId",
                table: "UserTokens",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
