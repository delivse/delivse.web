﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace delivse.web.Migrations
{
    public partial class RefactoredCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cardbilling_User_UserId",
                table: "Cardbilling");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_User_UserId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_User_UserId1",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_UserId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_UserId1",
                table: "Usermanagement");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Payment_UserId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_UserId",
                table: "Usermanagement");

            migrationBuilder.DropColumn(
                name: "IsCancelled",
                table: "TripTracker");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Tripmanagement",
                newName: "CustomerId");

            migrationBuilder.RenameColumn(
                name: "UserId1",
                table: "Payment",
                newName: "CustomerId1");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Payment",
                newName: "CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_Payment_UserId1",
                table: "Payment",
                newName: "IX_Payment_CustomerId1");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Cardbilling",
                newName: "CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_Cardbilling_UserId",
                table: "Cardbilling",
                newName: "IX_Cardbilling_CustomerId");

            migrationBuilder.RenameColumn(
                name: "UserId1",
                table: "Usermanagement",
                newName: "CustomerId1");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Usermanagement",
                newName: "CustomerId");

            migrationBuilder.RenameIndex(
                name: "IX_Usermanagement_UserId1",
                table: "Usermanagement",
                newName: "IX_Usermanagement_CustomerId1");

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeTaken",
                table: "TripTracker",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCancelled",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsScheduled",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "ScheduledTime",
                table: "Tripmanagement",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelivered",
                table: "GeneratedTripKey",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    CustomerId = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_QuanUser_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "QuanUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_CustomerId",
                table: "Tripmanagement",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_CustomerId",
                table: "Payment",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_CustomerId",
                table: "Usermanagement",
                column: "CustomerId",
                unique: true,
                filter: "[CustomerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_CustomerId",
                table: "Customer",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cardbilling_Customer_CustomerId",
                table: "Cardbilling",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_Customer_CustomerId",
                table: "Payment",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_Customer_CustomerId1",
                table: "Payment",
                column: "CustomerId1",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_Customer_CustomerId",
                table: "Tripmanagement",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_Customer_CustomerId",
                table: "Usermanagement",
                column: "CustomerId",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_Customer_CustomerId1",
                table: "Usermanagement",
                column: "CustomerId1",
                principalTable: "Customer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cardbilling_Customer_CustomerId",
                table: "Cardbilling");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_Customer_CustomerId",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Payment_Customer_CustomerId1",
                table: "Payment");

            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_Customer_CustomerId",
                table: "Tripmanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_Customer_CustomerId",
                table: "Usermanagement");

            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_Customer_CustomerId1",
                table: "Usermanagement");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_CustomerId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Payment_CustomerId",
                table: "Payment");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_CustomerId",
                table: "Usermanagement");

            migrationBuilder.DropColumn(
                name: "TimeTaken",
                table: "TripTracker");

            migrationBuilder.DropColumn(
                name: "IsCancelled",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "IsScheduled",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "ScheduledTime",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "IsDelivered",
                table: "GeneratedTripKey");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Tripmanagement",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "CustomerId1",
                table: "Payment",
                newName: "UserId1");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Payment",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Payment_CustomerId1",
                table: "Payment",
                newName: "IX_Payment_UserId1");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Cardbilling",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Cardbilling_CustomerId",
                table: "Cardbilling",
                newName: "IX_Cardbilling_UserId");

            migrationBuilder.RenameColumn(
                name: "CustomerId1",
                table: "Usermanagement",
                newName: "UserId1");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Usermanagement",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Usermanagement_CustomerId1",
                table: "Usermanagement",
                newName: "IX_Usermanagement_UserId1");

            migrationBuilder.AddColumn<bool>(
                name: "IsCancelled",
                table: "TripTracker",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_QuanUser_UserId",
                        column: x => x.UserId,
                        principalTable: "QuanUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Payment_UserId",
                table: "Payment",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_UserId",
                table: "Usermanagement",
                column: "UserId",
                unique: true,
                filter: "[UserId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserId",
                table: "User",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cardbilling_User_UserId",
                table: "Cardbilling",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_User_UserId",
                table: "Payment",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payment_User_UserId1",
                table: "Payment",
                column: "UserId1",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_UserId",
                table: "Usermanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_UserId1",
                table: "Usermanagement",
                column: "UserId1",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
