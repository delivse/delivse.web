﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace delivse.web.Migrations
{
    public partial class UserIdFK_TripManagement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_User_DriverId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_DriverId",
                table: "Tripmanagement");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tripmanagement_User_UserId",
                table: "Tripmanagement");

            migrationBuilder.DropIndex(
                name: "IX_Tripmanagement_UserId",
                table: "Tripmanagement");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DriverId",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tripmanagement_DriverId",
                table: "Tripmanagement",
                column: "DriverId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tripmanagement_User_DriverId",
                table: "Tripmanagement",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
