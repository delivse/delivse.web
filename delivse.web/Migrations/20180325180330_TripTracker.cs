﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace delivse.web.Migrations
{
    public partial class TripTracker : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Bill",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "Distance",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "IsCancelled",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "IsDelivered",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "IsPaid",
                table: "Tripmanagement");

            migrationBuilder.AddColumn<string>(
                name: "DriverId",
                table: "Usermanagement",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginLongitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OriginLatitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DestinationLongitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DestinationLatitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Time",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Weather",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<decimal>(
                name: "Longitude",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Latitude",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "TripTracker",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Bill = table.Column<decimal>(nullable: true),
                    Distance = table.Column<decimal>(nullable: true),
                    IsCancelled = table.Column<bool>(nullable: false),
                    IsDelivered = table.Column<bool>(nullable: false),
                    IsPaid = table.Column<bool>(nullable: false),
                    TripManagementId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripTracker", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TripTracker_Tripmanagement_TripManagementId",
                        column: x => x.TripManagementId,
                        principalTable: "Tripmanagement",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Usermanagement_DriverId",
                table: "Usermanagement",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_TripTracker_TripManagementId",
                table: "TripTracker",
                column: "TripManagementId");

            migrationBuilder.AddForeignKey(
                name: "FK_Usermanagement_User_DriverId",
                table: "Usermanagement",
                column: "DriverId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Usermanagement_User_DriverId",
                table: "Usermanagement");

            migrationBuilder.DropTable(
                name: "TripTracker");

            migrationBuilder.DropIndex(
                name: "IX_Usermanagement_DriverId",
                table: "Usermanagement");

            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "Usermanagement");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "Tripmanagement");

            migrationBuilder.DropColumn(
                name: "Weather",
                table: "Tripmanagement");

            migrationBuilder.AlterColumn<string>(
                name: "OriginLongitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OriginLatitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DestinationLongitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "DestinationLatitude",
                table: "Tripmanagement",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Bill",
                table: "Tripmanagement",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Distance",
                table: "Tripmanagement",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCancelled",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelivered",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPaid",
                table: "Tripmanagement",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "Longitude",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Latitude",
                table: "DriverPM",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
