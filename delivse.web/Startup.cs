﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using delivse.web.DAL;
using delivse.web.Models;
using delivse.web.Provider;
using delivse.web.Repositories;
using delivse.web.Services;

namespace delivse.web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            var builder = new ConfigurationBuilder();
            builder.AddUserSecrets<Startup>();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddDbContext<DelviseContext>(options => options.UseSqlServer(Configuration.GetConnectionString("Quan")));
            //Authentication Security
            services.AddIdentity<DelviseUser, IdentityRole>().AddEntityFrameworkStores<DelviseContext>().AddDefaultTokenProviders();

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                cfg.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = Configuration["JwtIssuer"],
                    ValidAudience = Configuration["JwtIssuer"],
                    ValidateLifetime=true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                    ClockSkew = TimeSpan.Zero
                };
            });

            //DAL and Repositories
            
            services.AddTransient<IDriverPositionManagement, DriverPositionManagementRepo>();
            services.AddTransient<IDriverManagement, DriverManagementRepo>();
            services.AddTransient<IPayment, PaymentRepo>();
            services.AddTransient<ITripManagement, TripManagementRepo>();
            services.AddTransient<ICustomerManagement, CustomerManagementRepo>();
            services.AddTransient<ITripTracker, TripTrackerRepo>();
            services.AddTransient<IGeneratedTripKey, GeneratedKeyRepository>();
            services.AddTransient<ICustomer, CustomerRepository>();
            services.AddTransient<IDriver, DriverRepository>();
            services.AddTransient<ICreditCard, CreditCardRepo>();
            services.AddScoped<AuthenticationProvider>();

            //Add Services
            services.AddScoped<ICalculateDistance, CalculateDistanceService>();
            services.AddScoped<INearestDriver, NearestDriverService>();
            services.AddScoped<IPriceCalculator, PriceCalculatorService>();
            services.AddScoped<IEncrypt, AESEncryptionService>();
            services.AddScoped<GenerateIDService>();
            services.AddAutoMapper();
            services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
