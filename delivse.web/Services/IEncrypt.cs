﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Services
{
    public interface IEncrypt
    {
        Task<byte[]> Encrypt( string entity);
        Task<string> Decrypt( byte[] entity);
    }
}
