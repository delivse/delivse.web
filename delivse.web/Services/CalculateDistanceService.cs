﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Services
{
    public class CalculateDistanceService : ICalculateDistance
    {
        private const decimal Radian = 6371.00M;
        public decimal GetDistance(decimal Longitudefrom, decimal Latitudefrom, decimal LongitudeTo, decimal LatitudeTo)
        {
            decimal dLong = Radians(LongitudeTo - Longitudefrom);
            decimal dLat = Radians(LatitudeTo - Latitudefrom);
            decimal lat1 = Radians(Latitudefrom);
            decimal lat2 = Radians(LatitudeTo);

            decimal a = (decimal)(Math.Pow(Math.Sin(decimal.ToDouble(dLat) / 2), 2) + (Math.Cos(decimal.ToDouble(lat1) / 2)) * (Math.Cos(decimal.ToDouble(lat2)/2)) *
                                    (Math.Pow(Math.Sin(decimal.ToDouble(dLong) / 2), 2)));

            decimal c = (decimal)(2 * Math.Atan2(Math.Sqrt(decimal.ToDouble(a)),Math.Sqrt(1-decimal.ToDouble(a))));

            decimal distance = (decimal)(Radian * c);
            return Math.Round(distance,3);

            //local function
            decimal Radians(decimal value)
            {
                return value * (decimal)(Math.PI / 180);
            }
        }

        
    }
}
