﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Services
{
    public interface ICalculateDistance
    {
        decimal GetDistance(decimal Longitudefrom, decimal Latitudefrom, decimal LongitudeTo, decimal LatitudeTo);
        
    }
}
