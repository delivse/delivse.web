﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using delivse.web.Models;
using delivse.web.Repositories;

namespace delivse.web.Services
{
    public class PriceCalculatorService : IPriceCalculator
    {
        private delegate decimal DistanceDel(decimal Longitudefrom, decimal Latitudefrom, decimal LongitudeTo, decimal LatitudeTo);
        private const double Fixedprice = 70.00;
        private const double Discount = 15.00;

        private ITripManagement tripManagement;
        DistanceDel distanceDel;

        public PriceCalculatorService(ITripManagement tripManagement,
                    ICalculateDistance distance)
        {
            this.tripManagement = tripManagement;
            this.distanceDel= distance.GetDistance;
        }

        public async Task<double> PriceCalculator(Guid Id)
        {
            // factors
            var trip = await this.tripManagement.GetTrip(Id);

            double costfactors  = await PriceFactors(trip);
            
            double surgecost = await SurgePrice(trip);
            double totalcost = costfactors + surgecost + Fixedprice;

            if (trip.ToTime != null)
            {
                totalcost = totalcost - Discount;
            }

            return totalcost;

            //Local Funtion: Price factors
            async Task<double> PriceFactors(TripManagement tripManagement)
            {
                TimeSpan? timetaken = null;
                Decimal distance = 0.00M;
                if (tripManagement.ToTime != null && tripManagement.DestinationLatitude != 0.00M && tripManagement.DestinationLongitude != 0.00M)
                {
                    timetaken = (tripManagement.ToTime - tripManagement.FromTime);
                    distance = this.distanceDel(tripManagement.OriginLongitude, tripManagement.OriginLatitude, tripManagement.DestinationLongitude, tripManagement.DestinationLatitude);
                }
                //time price factor
                double cost = (timetaken.Value.Minutes * 0.75) + (double)(distance * 75);
                return cost;
            }

            //Local Function: Surge Price
            async Task<double> SurgePrice(TripManagement tripManagement)
            {
                var timeofDay = tripManagement.Time;
                var weather = tripManagement.Weather;
                double timecost = 0.00;
                double weathercost = 0.00;

                if (timeofDay.ToString() == "DayTime")
                {
                    timecost = 8.20;
                }
                else if (timecost.ToString() == "NightTime")
                {
                    timecost = 25.25;
                }

                if (weather.ToString() == "Calm")
                {
                    weathercost = 5.20;
                }
                else if (weather.ToString() == "Rainy")
                {
                    weathercost = 27.20;
                }

                return timecost + weathercost;
            }
        }
        
    }
}
