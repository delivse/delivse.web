﻿using delivse.web.Models;
using delivse.web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Services
{
    /// <summary>
    /// Get the Driver phone numbers near by
    /// </summary>
    public interface INearestDriver
    {
        Task<ICollection<Guid>> FindNearestDriver(decimal Longitude, decimal Latitude);
    }
}
