﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace delivse.web.Services
{
    /// <summary>
    /// AES encryption
    /// </summary>
    public class AESEncryptionService : IEncrypt
    {
        private string Key; 
        public AESEncryptionService(string Key)
        {
            this.Key = Key;
        }
        private static IConfigurationRoot Configuration { get; }


        private static string EncryptionKey = Configuration["EncryptionKey"];

        public async Task<string> Decrypt(byte[] entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException("Empty Entity");
            }
            if(Key.Length >0 && Key.Length <= 3)
            {
                //add extra security
                EncryptionKey = EncryptionKey + Key;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Key should be between 0 and 3");
            }

            //0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
            string Decrypted = String.Empty;

            using(Aes aesAlg = Aes.Create())
            {
                Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64,
                                                                                            0x76, 0x76, 0x61, 0x65, 0x64, 0x65, 0x76, 0x61,
                                                                                            0x6e, 0x20, 0x4d, 0x61, 0x6e, 0x20, 0x76, 0x76 });

                aesAlg.Key = rfc.GetBytes(32);
                aesAlg.IV = rfc.GetBytes(16);
                //
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using(MemoryStream memDecrypt = new MemoryStream(entity))
                {
                    using(CryptoStream csDecrypt = new CryptoStream(memDecrypt,decryptor,CryptoStreamMode.Read))
                    {
                        using(StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            Decrypted = await srDecrypt.ReadToEndAsync();
                        }
                    }
                }
            }

            return Decrypted;
        }

        public async Task<byte[]> Encrypt(string entity)
        {
            if(entity == String.Empty && entity.Length < 3)
            {
                throw new ArgumentNullException("Empty entity");
            }
            else
            {
                var key = entity.TakeLast<char>(3).ToString();
                EncryptionKey = EncryptionKey + key;
            }

            byte[] Encrypted;

            using(Aes aesAlg = Aes.Create())
            {
                Rfc2898DeriveBytes rfc = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64,
                                                                                            0x76, 0x76, 0x61, 0x65, 0x64, 0x65, 0x76, 0x61,
                                                                                            0x6e, 0x20, 0x4d, 0x61, 0x6e, 0x20, 0x76, 0x76  });
                aesAlg.Key = rfc.GetBytes(32);
                aesAlg.IV = rfc.GetBytes(16);

                //Create an Encryptor
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using(MemoryStream msEncrypt = new MemoryStream())
                {
                    using(CryptoStream csStream = new CryptoStream(msEncrypt,encryptor,CryptoStreamMode.Write))
                    {
                        using(StreamWriter streamWriter = new StreamWriter(csStream))
                        {
                            await streamWriter.WriteAsync(entity);
                            streamWriter.Close();
                        }
                        Encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return Encrypted;
        }
    }
}
