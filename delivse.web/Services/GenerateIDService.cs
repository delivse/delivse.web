﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace delivse.web.Services
{
    /// <summary>
    /// Generate Unique ID's for Drivers and Trip Termination
    /// </summary>
    public class GenerateIDService
    {
        public string GenerateDriverID()
        {
            Guid guid = Guid.NewGuid();
            string Id = guid.ToString().Take<char>(6).ToString();

            return Id.ToUpper();
        }

        public string GenerateTripID(string driverUID)
        {
            Guid guid = Guid.Empty;

            using (MD5 md5 = MD5.Create())
            {
                byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(driverUID));
                guid = new Guid(hash);
            }
            
            dynamic Id = guid.ToString().TakeLast<char>(5).ToString();
            return Id.ToUpper();
        }
    }
}
