﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using delivse.web.DAL;
using delivse.web.Models.DTO;
using delivse.web.Repositories;

namespace delivse.web.Services
{
    /// <summary>
    /// Calculate nearest driver and return top 5
    /// </summary>
    public class NearestDriverService : INearestDriver
    {
        private delegate decimal DistanceDelegate(decimal Longitudefrom, decimal Latitudefrom, decimal LongitudeTo, decimal LatitudeTo);
        
        private IDriverPositionManagement driverPosition;
        DistanceDelegate @delegate;
        public NearestDriverService(ICalculateDistance distance,
                                     IDriverPositionManagement driverPosition)
        {
            this.@delegate = distance.GetDistance;
            this.driverPosition = driverPosition;
        }

        public async Task<ICollection<Guid>> FindNearestDriver(decimal Longitude, decimal Latitude)
        {
            var driversposnearby = await this.driverPosition.GetUnassignedDrivers();
            Dictionary<Guid,decimal> distances = new Dictionary<Guid,decimal>();

            foreach(var driverpos in driversposnearby)
            {
                decimal dis = this.@delegate(Longitude, Latitude, driverpos.Longitude, driverpos.Latitude);
                distances.Add(driverpos.DriverId, dis);
            }

            Dictionary<Guid, decimal> nearestDrivers = distances.OrderByDescending(x => x.Value).TakeLast(5)
                                    .ToDictionary(c => c.Key, c => c.Value);
            List<Guid> driverIDs = nearestDrivers.Keys.ToList();

            return driverIDs;
        }
    }
}
