﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Provider;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivse.web.Controllers
{
    public class AccountsController : Controller
    {
        private delegate object AuthDelegate(string email, DelviseUser user);

        private readonly UserManager<DelviseUser> userManager;
        private readonly SignInManager<DelviseUser> signInManager;
        private readonly IConfiguration configuration;
        AuthDelegate @delegate;

        public AccountsController(UserManager<DelviseUser> userManager,
                                    SignInManager<DelviseUser> signInManager,
                                     IConfiguration configuration,
                                     AuthenticationProvider authProvider)
        {
            this.configuration = configuration;
            this.userManager = userManager;
            this.signInManager = signInManager;
            @delegate = authProvider.GenerateJwtToken;
        }
        // GET: api/<controller>
        [HttpPost]
        [Route("api/login")]
        public async Task<object> Login(LoginDTO login)
        {
            if (ModelState.IsValid)
            {
                var result = await this.signInManager.PasswordSignInAsync(login.Email, login.Password, login.IsPersistent, false);

                if (result.Succeeded)
                {
                    var user = await userManager.Users.Where(x => x.Email == login.Email || x.UserName == login.Email).SingleOrDefaultAsync();
                    return @delegate(user.Email, user);
                }
            }
            throw new ApplicationException("Failed Login");
        }
        
    }
}
