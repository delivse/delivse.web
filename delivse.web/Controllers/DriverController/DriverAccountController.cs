﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Provider;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivse.web.Controllers.DriverController
{
    [Route("api/[controller]")]
    public class DriverAccountController : Controller
    {
        private delegate object AuthDelegate(string email, DelviseUser user);

        private UserManager<DelviseUser> userManager;
        private SignInManager<DelviseUser> signInManager;
        private RoleManager<IdentityRole> roleManager;
        private IConfiguration configuration;
        private IMapper mapper;
        AuthDelegate @delegate;

        public DriverAccountController(UserManager<DelviseUser> userManager,
                                     SignInManager<DelviseUser> signInManager,
                                     RoleManager<IdentityRole> roleManager,
                                     IConfiguration configuration,
                                     IMapper mapper,
                                     AuthenticationProvider authProvider)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.configuration = configuration;
            this.mapper = mapper;
            @delegate = authProvider.GenerateJwtToken;
        }


        [HttpPost]
        [Route("/register")]
        public async Task<object> Register(RegisterDTO register)
        {

            if (ModelState.IsValid)
            {
                var user = this.mapper.Map<DelviseUser>(register);

                var result = this.userManager.CreateAsync(user, register.Password);

                if (result.IsCompletedSuccessfully)
                {
                    await this.signInManager.SignInAsync(user, false);
                    if(!await this.roleManager.RoleExistsAsync("Driver"))
                    {
                        await this.roleManager.CreateAsync(new IdentityRole { Name = "Driver", NormalizedName="DRIVER" });
                    }
                    if(!await this.userManager.IsInRoleAsync(user, "Driver"))
                    {
                            await this.userManager.AddToRoleAsync(user, "Driver");
                    }
                    
                    return @delegate(user.Email, user);
                }
            }
            throw new ApplicationException("Register Failed");
        }
      
    }
}
