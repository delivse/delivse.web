﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using delivse.web.Services;

namespace delivse.web.Controllers.DriverController
{
    [Authorize(Roles ="Driver")]
    [Produces("application/json")]
    [Route("api/DriverTrip")]
    public class DriverTripManagementController : Controller
    {
        private UserManager<DelviseUser> userManager;
        private ITripManagement tripManagement;
        private IDriverPositionManagement driverPosition;
        private INearestDriver nearestDriver;
        public DriverTripManagementController(UserManager<DelviseUser> userManager,
                                    ITripManagement tripManagement,
                                    IDriverPositionManagement driverPosition,
                                    INearestDriver nearestDriver)
        {
            this.userManager = userManager;
            this.driverPosition = driverPosition;
            this.tripManagement = tripManagement;
            this.nearestDriver = nearestDriver;
        }
        // SET: the Driver to a trip
        // GET: api/DriverTrip
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetTrip()
        {
            //Get a trip unassigned Trip
            var trip = await this.tripManagement.GetUnassignedTrip();
            //Find the nearest logged in User
            if (trip != null)
            {
                var unassignedDriver = await this.nearestDriver.FindNearestDriver(trip.OriginLongitude, trip.OriginLatitude);
                foreach(var undriver in unassignedDriver)
                {
                    var driver = await this.userManager.FindByIdAsync(ClaimTypes.NameIdentifier);

                    if (undriver.Equals(driver.Id))
                    {
                        return Ok(trip);
                    }
                    return NoContent();
                }
            }
            return NotFound("No trips available");
            
        }
        
        
        [HttpPut("{Id:Guid}")]
        public async Task<IActionResult> DriverAccept(Guid Id, [FromBody]TripManagementDTO tripManagement)
        {
            if (ModelState.IsValid)
            {
                var trip = await this.tripManagement.Get(Id);
                if(trip != null)
                {
                   if (tripManagement.IsHappening == true)
                    {
                        var driver = await this.userManager.FindByIdAsync(ClaimTypes.NameIdentifier);
                        //set driver 
                        trip.DriverId = tripManagement.DriverId;
                        trip.IsHappening = tripManagement.IsHappening;
                        trip.Weather = tripManagement.Weather;
                        trip.Time = tripManagement.Time;
                        //update the trip
                       // await this.tripManagement.Update(trip);
                        return Ok("Trip Accepted");
                    }
                   //If trip isn't happening 
                    return BadRequest("Trip Unavailable");
                }
                return NotFound("Entity Unavailable");
            }
            return BadRequest("Model State Error");
        }
        
        [HttpPost("{id:Guid}")]
        public async Task<IActionResult> CompleteTrip(Guid Id, string TripToken)
        {
            var trip = await this.tripManagement.GetHappeningTrip(Id);
            if(trip != null)
            {
                if(trip.TripToken.Equals(TripToken))
                {
                    trip.IsHappening = false;
                    trip.IsCompleted = true;
                    //update trip
                    //await this.tripManagement.Update(trip);
                    return Ok("Trip Completed");
                }
                return BadRequest("UnMatched Tokens");
            }
            return NotFound("Entity Unavailable");
        }
    }
}
