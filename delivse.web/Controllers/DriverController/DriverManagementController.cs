﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;

namespace delivse.web.Controllers.DriverController
{
    [Authorize(Roles ="Driver")]
    [Produces("application/json")]
    [Route("api/DriverManagement")]
    public class DriverManagementController : Controller
    {
        private IDriverManagement driverManagement;
        private IDriverPositionManagement positionManagement;
        private UserManager<DelviseUser> userManager;
        public DriverManagementController(IDriverManagement driverManagement, 
                                          IDriverPositionManagement positionManagement,
                                          UserManager<DelviseUser> userManager)
        {
            this.driverManagement = driverManagement;
            this.positionManagement = positionManagement;
            this.userManager = userManager;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DriverManagementDTO, DriverManagement>()
                    .ForMember(opt => opt.DriverId, dest => dest.Ignore())
                    .ForMember(opt => opt.Driver, dest => dest.Ignore())
                    .ForMember(opt => opt.Id, dest => dest.Ignore())
                    .ForMember(opt => opt.Time, dest => dest.Ignore());
                cfg.CreateMap<DriverPositionDTO, DriverPositionManagement>()
                    .ForMember(opt => opt.DriverId, dest => dest.Ignore())
                    .ForMember(opt => opt.Driver, dest => dest.Ignore())
                    .ForMember(opt => opt.Id, dest => dest.Ignore())
                    .ForMember(opt => opt.IsDelivering, dest => dest.Ignore())
                    .ForMember(opt => opt.IsStopped, dest => dest.Ignore());
            });
            Mapper.AssertConfigurationIsValid();
        }
        // POST: api/DriverManagement
        [HttpPost]
        [Route("/active")]
        public async Task<IActionResult> Active([FromBody]DriverManagementDTO driverManagement)
        {
            if (ModelState.IsValid)
            {
                var driver = await this.userManager.FindByIdAsync(ClaimTypes.NameIdentifier);
                var drivermngnt = Mapper.Map<DriverManagement>(driverManagement);
                drivermngnt.DriverId = driver.Id;
                await this.driverManagement.Add(drivermngnt);
                return Ok();
            }
            return BadRequest("Wrong Entity State");
        }

        [HttpPost]
        [Route("/position")]
        public async Task<IActionResult> Position([FromBody]DriverPositionDTO driverPosition)
        {
            if (ModelState.IsValid)
            {
                var driver = await this.userManager.FindByIdAsync(ClaimTypes.NameIdentifier);
                var driverPos = Mapper.Map<DriverPositionManagement>(driverPosition);
                driverPos.DriverId = driver.Id;
                await this.positionManagement.Add(driverPos);
                //too many request return nothing
                return null;
            }
            return BadRequest("Bad State");
        }
    }
}
