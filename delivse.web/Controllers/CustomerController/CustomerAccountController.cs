﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Provider;
using delivse.web.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivse.web.Controllers.UserController
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CustomerAccountController : Controller
    {
        private delegate object AuthDelegate(string email, DelviseUser user);

        private UserManager<DelviseUser> userManager;
        private SignInManager<DelviseUser> signInManager;
        private RoleManager<DelviseRole> roleManager;
        private ICustomer customer;
        AuthenticationProvider authenticationProvider;
        AuthDelegate @delegate;

        public CustomerAccountController(UserManager<DelviseUser> userManager,
                                    SignInManager<DelviseUser> signInManager,
                                    RoleManager<DelviseRole> roleManager,
                                    ICustomer customer
                                    )
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
            this.customer = customer;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CustomerDTO, Customer>();
                cfg.CreateMap<RegisterDTO, DelviseUser>();
            });
            Mapper.AssertConfigurationIsValid();
        }

        [HttpPost]
        [Route("/register")]
        public async Task<object> Register(RegisterDTO register)
        {

            if (ModelState.IsValid)
            {
                //MAP
                var customer = Mapper.Map<DelviseUser>(register);

                var result = this.userManager.CreateAsync(customer, register.Password);
                this.@delegate = this.authenticationProvider.GenerateJwtToken;
                if (result.IsCompletedSuccessfully)
                {
                    await this.signInManager.SignInAsync(customer, false);
                    if(!await this.roleManager.RoleExistsAsync("Customer"))
                    {
                        await this.roleManager.CreateAsync(new DelviseRole { Name = "Customer", NormalizedName = "CUSTOMER" });
                    }
                    if(!await this.userManager.IsInRoleAsync(customer, "Customer"))
                    {
                        await this.userManager.AddToRoleAsync(customer, "Customer");
                    }
                    return @delegate(customer.Email, customer);
                }
            }
            throw new ApplicationException("Register Failed");
        }
       
        [HttpPost]
        [Route("")]
        [Authorize(Roles ="Customer")]
        public async Task<IActionResult> CreateProfile([FromBody]CustomerDTO customer_)
        {
            if (ModelState.IsValid)
            {
                var cust = Mapper.Map<Customer>(customer_);
                await this.customer.Add(cust);
                return CreatedAtRoute(routeName: "GetProfile", routeValues: new { customer_.Id }, value: customer);
            }
            return BadRequest("Failed to Create Profile");
        }

        [HttpGet]
        [Route("{id:guid}", Name ="GetProfile")]
        [Authorize(Roles ="Customer")]
        public async Task<IActionResult> GetProfile(Guid Id)
        {
            var profile = await this.customer.GetUser(Id);

            if (profile != null)
            {
                return Ok(profile);
            }
            return NotFound("Unavailable");
        }

        [HttpPut]
        [Route("{id:guid}")]
        [Authorize(Roles ="Customer")]
        public async Task<IActionResult> UpdateProfile(Guid Id, CustomerDTO customer_)
        {
            if (ModelState.IsValid)
            {
                var cust = await this.customer.GetUser(Id);
                if(cust != null)
                {
                    var cst = Mapper.Map<Customer>(customer_);
                    await this.customer.Update(cst);
                    return Ok();
                }
            }
            return BadRequest("Failed to Update");
        }
    }
}
