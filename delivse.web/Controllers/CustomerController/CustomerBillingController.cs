﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using delivse.web.Services;
 
namespace delivse.web.Controllers.CustomerController
{

    [Authorize(Roles ="Customers")]
    [Produces("application/json")]
    [Route("api/CustomerBilling")]
    public class CustomerBillingController : Controller
    {
        private ICreditCard creditCard;
        private AESEncryptionService encryptionService;
        public CustomerBillingController(ICreditCard creditCard)
        {
            this.creditCard = creditCard;
        }

       [Route("{id:guid}")]
       [HttpPost]
       public async Task<IActionResult> Create(Guid Id, CreditCardDTO creditCard)
        {

            if (ModelState.IsValid)
            {
                var Key = creditCard.CardNumber.TakeLast<char>(3).ToString();
                encryptionService = new AESEncryptionService(Key);
                
                //Encrypt the Card
                byte[] cardnumber = await encryptionService.Encrypt(creditCard.CardNumber);
                byte[] cvv = await encryptionService.Encrypt(creditCard.CVV);

                var credit = new CardBilling
                {
                    AccountName = creditCard.AccountName,
                    CardNumber = cardnumber,
                    CustomerId = Id,
                    CVV = cvv,
                    ExpiryDate = creditCard.ExpiryDate,
                    PostalCode = creditCard.PostalCode,
                    ShortKey = Key
                };

                await this.creditCard.Add(credit);
                return Ok("Successfully Added");
            }
            return BadRequest("Wrong Entity Format");
        }


        [Route("creditcard/{id:guid}")]
        [HttpPost]
        public async Task<IActionResult> CreditCardCredentials(Guid Id, string Key)
        {
            if(Key != string.Empty)
            {
                var card = await this.creditCard.Get(Id);
                if(card != null)
                {
                    //try decrypting credit card...
                    encryptionService = new AESEncryptionService(Key);
                    string CardNumber = await encryptionService.Decrypt(card.CardNumber);
                    
                    if(CardNumber == string.Empty)
                    {
                        return BadRequest("Wrong Input");
                    }
                    return Ok(CardNumber);
                }
                return NotFound("Entity Missing");
            }

            return BadRequest("Key Can't be Empty");
        }

        [Route("{id:guid}")]
        [HttpPut]
        public async Task<IActionResult> Update(Guid Id, CardBilling creditCard)
        {
            if (ModelState.IsValid)
            {
                var card = await this.creditCard.Get(Id);
                if(card != null)
                {
                    //update card 
                    //await this.creditCard.Update(creditCard);
                    return Ok();
                }
                return NotFound("Card doesn't exist");
            }
            return BadRequest("Entity Error");
        }

        [Route("{id:guid}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteCard(Guid Id)
        {
            var card = await this.creditCard.Get(Id);
            if (card != null)
            {

                this.creditCard.Remove(card);
                return Ok();
            }
            return BadRequest("Entity Is Unavailable");
        }
    }
}
