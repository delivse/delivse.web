﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using delivse.web.Services;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using delivse.web.DAL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivse.web.Controllers.UserController
{
    [Authorize(Roles = "Customer")]
    [Produces("application/json")]
    [Route("api/CustomerTripManagement")]
    public class CustomerTripManagementController : Controller
    {
        private delegate string GenerateID(string driverUID);

        private ITripManagement tripManagement;
        private ITripTracker tripTracker;
        private IDriver driver;
        private IDriverPositionManagement driverPosition;
        private ICustomerManagement customerManagement;
        private UserManager<DelviseUser> userManager;
        private GenerateIDService GenerateIDService_ = new GenerateIDService();

        public CustomerTripManagementController(ITripManagement tripManagement,
                                            ITripTracker tripTracker,
                                            IDriver driver,
                                            IDriverPositionManagement driverPosition,
                                            ICustomerManagement customerManagement,
                                            UserManager<DelviseUser> userManager
                                            )
        {
            this.tripTracker = tripTracker;
            this.tripManagement = tripManagement;
            this.userManager = userManager;
            this.driver = driver;
            this.driverPosition = driverPosition;
            this.customerManagement = customerManagement;

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<TripManagementDTO, Models.TripManagement>().ReverseMap();
                cfg.CreateMap<CustomerTripManagementDTO, Models.TripManagement>()
                    .ForMember(x => x.Id, opt => opt.Ignore())
                    .ForMember(x => x.DriverId, opt => opt.Ignore())
                    .ForMember(x => x.Driver, opt => opt.Ignore())
                    .ForMember(x => x.Customer, opt => opt.Ignore())
                    .ForMember(x => x.IsHappening, opt => opt.Ignore())
                    .ForMember(x => x.TripToken, opt => opt.Ignore())
                    .ForMember(x => x.Time, opt => opt.Ignore())
                    .ForMember(x => x.Weather, opt => opt.Ignore());
            });
            Mapper.AssertConfigurationIsValid();
        }


        //GET api/<controller>
        [HttpGet]
        [Route("usertrips/{id:guid}")]
        public async Task<IActionResult> GetTrips(Guid? Id)
        {
            
            //Map the results
            if (Id != null)
            {
                var tripsbyUser = await this.tripManagement.GetTripsByUser(Id.Value);
                if (tripsbyUser != null)
                {
                    var trips = Mapper.Map<ICollection<TripManagementDTO>>(tripsbyUser);
                    return Ok(trips);
                }
                return NotFound("Entity Unavailable");
            }

            return NoContent();
        }

        //GET api/<controller>
        [HttpGet]
        [Route("usertrip/{id:guid}", Name = "GetTrip")]
        public async Task<IActionResult> GetTrip(Guid? Id)
        {
            
            if (Id != null)
            {
                //Fetch a recent/current Trip
                var trip = await this.tripManagement.GetTripByUser(Id.Value);
                if(trip != null)
                {
                    var recentTrip = Mapper.Map<TripManagementDTO>(trip);
                    return Ok(recentTrip);
                }
                return NotFound("Entity Unavailable");
            }

            return NoContent();
        }

        //POST api/<controller>
        [HttpPost]
        [Route("/create_trip")]
        public async Task<IActionResult> CreateTrip([FromBody]CustomerTripManagementDTO customerTripManagement)
        {
            //create Trip 
            var user = await userManager.FindByIdAsync(ClaimTypes.NameIdentifier);

            //TODO: Generate an PUSH alert
            if (ModelState.IsValid)
            {
                customerTripManagement.CustomerId = user.Id;
                //Map
                var customertrip = Mapper.Map<Models.TripManagement>(customerTripManagement);
                await this.tripManagement.Add(customertrip);
                return CreatedAtRoute(routeName: "GetTrip", routeValues: new { customertrip.Id }, value: customertrip);
            }
            return BadRequest("Failed to Add");
        }

        [HttpPut]
        [Route("/start_trip/{id:guid}")]
        public async Task<IActionResult> StartTrip(Guid Id, [FromBody]TripManagementDTO tripManagement)
        {
            //get trip
            if (ModelState.IsValid)
            {
               //get driver by Id
               var trip = Mapper.Map<TripManagement>(tripManagement);
               //TODO: Driver Delivering.
               var driver_ = await this.driver.Get(trip.DriverId.Value);
               if (driver_ != null)
               {
                  //Trip ID
                  string tripUID = GenerateIDService_.GenerateTripID(driver_.GeneratedID);
                  trip.TripToken = tripUID;
                  // trip.IsHappening = true;
                 // await this.tripManagement.Update(trip);
                  return Ok("Trip Started");
                }
                return NotFound("Entity Unavailable");
            }
            return BadRequest("Model State Error");
        }

        /// <summary>
        /// Update a trip to cancel it
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="tripManagement"></param>
        /// <returns></returns>

        [HttpPut]
        [Route("/cancel_trip/{id:guid}")]
        public async Task<IActionResult> CancelTrip(Guid Id, [FromBody]TripManagementDTO tripManagement)
        {
            if (ModelState.IsValid)
            {
                var trip = await this.tripManagement.Get(Id);
                if (trip != null)
                {
                    var tripmngnt = Mapper.Map<TripManagement>(tripManagement);
                    //await this.tripManagement.Update(tripmngnt);

                    //update drivers position
                    var driverPos = await this.driverPosition.GetRecentPosition(trip.DriverId.Value);
                    driverPos.IsStopped = true;
                    driverPos.IsDelivering = false;
                   // await this.driverPosition.Update(driverPos);
                    return Ok("Cancel Successful");
                }
                return NotFound("Entity Unavailable");
            }
            return BadRequest("Failed");
        }

        /// <summary>
        /// Get Status of a Trip
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Trip</returns>
        [HttpGet]
        [Route("/trip/{id:guid}")]
        public async Task<IActionResult> GetStatus(Guid Id)
        {
            var trip = await this.tripManagement.Get(Id);

            if (trip != null)
            {
                return Ok(trip);
            }
            return NotFound("Unavailable");
        }

        [HttpGet]
        [Route("/driverpos/{id:trip}")]
        public async ValueTask<IActionResult> GetDriverPosition(Guid Id)
        {
            var trip = await this.tripManagement.Get(Id);
            //get by driver Id
            if (trip != null)
            {
                var driverPos = await this.driverPosition.GetDriverCurrentPosition(trip.DriverId.Value);
                return Ok(driverPos);
            }
            return NotFound("Entity Unavailable");
        }

        [HttpGet]
        [Route("/driver/{id:guid}")]
        public async Task<IActionResult> GetDriverDetails(Guid Id)
        {
            var driver = await this.driver.Get(Id);
            if(driver != null)
            {
                var driverdetails = await this.customerManagement.GetDriverAverageRating(Id);
                return Ok(driverdetails);
            }
            return NotFound("Entity Unavailable");
        }
    }
}
