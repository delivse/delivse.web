﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using delivse.web.Models;
using delivse.web.Models.DTO;
using delivse.web.Repositories;
using System.Net;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace delivse.web.Controllers.UserController
{
    [Authorize(Roles ="Customer")]
    [Produces("application/json")]
    [Route("api/CustomerManagement")]
    public class CustomerManagementController : Controller
    {
        private ITripManagement tripManagement;
        private ICustomerManagement customerManagement;
 
        public CustomerManagementController(ITripManagement tripManagement,
                                            ICustomerManagement customerManagement)
        {
            this.tripManagement = tripManagement;
            this.customerManagement = customerManagement;
            
            Mapper.Initialize(cfg => cfg.CreateMap<CustomerManagement, CustomerManagementDTO>().ReverseMap());
            Mapper.AssertConfigurationIsValid();
        }

        // GET api/<controller>/5
        [HttpGet("{id:guid}")]
        [Route("",Name ="GetCustomerManagement")]
        public async Task<IActionResult> Get(Guid id)
        {
            var custManagement = await this.customerManagement.Get(id);

            if(custManagement != null)
            {
               var customerManagement = Mapper.Map<CustomerManagementDTO>(custManagement);
                return Ok(customerManagement);
            }
            return NotFound("Unavailable");
        }

        // POST api/<controller>
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody]CustomerManagementDTO customerMgnt)
        {
            
            if (ModelState.IsValid)
            {
                var trip = await this.tripManagement.Get(customerMgnt.TripId);
                if (trip != null)
                {
                    customerMgnt.DriverId = trip.DriverId;
                    customerMgnt.CustomerId = trip.CustomerId;

                    //Map
                    var management = Mapper.Map<CustomerManagement>(customerMgnt);

                    await this.customerManagement.Add(management);
                    return Ok();
                }
                return BadRequest("Entity is Non-Existent");
            }

            return BadRequest("Failed to Add");
        }

        // PUT api/<controller>/5
        [HttpPut("{id:guid}")]
        [Route("")]
        public async Task<IActionResult> Put(Guid id, [FromBody]CustomerManagementDTO managementDTO)
        {
            
            if (ModelState.IsValid)
            {
                var cstmanagement = await this.customerManagement.Get(id);
                if(cstmanagement != null)
                {
                    //update
                    var customerManagement = Mapper.Map<CustomerManagement>(managementDTO);
                    //await this.customerManagement.Update(customerManagement);
                    return AcceptedAtRoute(routeName:"GetCustomerManagement",routeValues: new { managementDTO.Id },value:managementDTO);
                }
                return NotFound("Entity is Non Existent");
            }
            return BadRequest("Failed to Update");
        }
        
    }
}
