﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Mappings.Resolvers
{
    public class DateTimeResolver : IValueResolver<object, object, DateTime>
    {
        public DateTime Resolve(object source, object destination, DateTime destMember, ResolutionContext context)
        {
            return DateTime.Now;
        }
    }
}
