﻿using AutoMapper;
using delivse.web.Controllers.UserController;
using delivse.web.Mappings.Resolvers;
using delivse.web.Models;
using delivse.web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Mappings
{
    /// <summary>
    /// Mapping Profile Inherits from Class Profile and Defines Mappings 
    /// </summary>
    public class MappingProfile: Profile
    {
        //Map Items
        public MappingProfile()
        {
            CreateMap<RegisterDTO, DelviseUser>();
            CreateMap<CustomerTripManagementDTO, Models.TripManagement>();
            CreateMap<Models.TripManagement, CustomerTripManagementDTO>();
            CreateMap<CustomerManagement, CustomerManagementDTO>();
            CreateMap<Customer, CustomerDTO>().ReverseMap();
            CreateMap<Models.TripManagement, TripManagementDTO>().ReverseMap();
            CreateMap<DriverManagementDTO, DriverManagement>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
                .ForMember(opt => opt.Time, dest => dest.ResolveUsing<DateTimeResolver>());
            CreateMap<DriverPositionDTO, DriverPositionManagement>()
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                .ForMember(opt => opt.Time, dest => dest.ResolveUsing<DateTimeResolver>());

            Mapper.AssertConfigurationIsValid();
        }
    }
}
