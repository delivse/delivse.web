﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    public partial class DriverPositionManagement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public Guid DriverId { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public bool IsDelivering { get; set; }
        public bool IsStopped { get; set; }
        public DateTime Time { get; set; }
    }

    public partial class DriverPositionManagement
    {
        [ForeignKey("DriverId")]
        public virtual Driver Driver { get; set; }
    }
}
