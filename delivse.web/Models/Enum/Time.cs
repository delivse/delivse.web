﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.Enum
{
    public enum Time
    {
        DayTime = 1,
        NightTime = 2
    }
}
