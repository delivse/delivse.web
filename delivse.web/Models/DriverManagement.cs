﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    public partial class DriverManagement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
       
        public DateTime Time { get; set; }
        public bool IsActive { get; set; }
        public Guid DriverId { get; set; }
    }

    public partial class DriverManagement
    {
        [ForeignKey("DriverId")]
        public virtual Driver Driver { get; set; }
    }
}
