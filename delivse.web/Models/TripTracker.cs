﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    public partial class TripTracker
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid TripManagementId { get; set; }
        public DateTime? TimeTaken { get; set; }
        public bool IsDelivered { get; set; }
        public decimal? Distance { get; set; }
        public decimal? Bill { get; set; }
    }
    public partial class TripTracker
    {
        [ForeignKey("TripManagementId")]
        public virtual TripManagement TripManagement { get; set; }
    }
}
