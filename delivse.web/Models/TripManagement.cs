﻿using delivse.web.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    /// <summary>
    /// Trip Management Data
    /// </summary>
    public partial class TripManagement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid? DriverId { get; set; }
        public Guid? CustomerId { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime? ToTime { get; set; }
        public DateTime? ScheduledTime { get; set;}
        public string ClientPhoneNumber { get; set; }
        public string Description { get; set;  }
        public bool IsScheduled { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsHappening { get; set; }
        public decimal OriginLatitude { get; set; }
        public decimal OriginLongitude { get; set; }
        public decimal DestinationLatitude { get; set; }
        public decimal DestinationLongitude { get; set; }
        public string TripToken { get; set; }
        public Time Time { get; set; }
        public Weather Weather { get; set; }
    }
    [Serializable]
    public partial class TripManagement
    {
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        [ForeignKey("DriverId")]
        public virtual Driver Driver { get; set; }
    }
}
