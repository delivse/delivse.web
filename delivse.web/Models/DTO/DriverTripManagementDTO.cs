﻿using delivse.web.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public class DriverTripManagementDTO
    {
        
        public Guid DriverId { get; set; }
        [Required]
        public bool IsHappening { get; set; }
        [Required]
        public Time Time { get; set; }
        [Required]
        public Weather Weather { get; set; }
    }
}
