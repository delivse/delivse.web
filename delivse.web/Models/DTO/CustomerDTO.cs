﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public class CustomerDTO
    {
        public Guid Id { get; set; }
        public string CustomerId { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string City { get; set; }
    }
}
