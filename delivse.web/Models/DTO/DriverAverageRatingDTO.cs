﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public class DriverAverageRatingDTO
    {
        public Guid DriverUID { get; set;}
        public string FullName { get; set; }
        public string Vehicle { get; set; }
        public double AverageRatings { get; set; }
    }
}
