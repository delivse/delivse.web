﻿using delivse.web.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public partial class TripManagementDTO
    {
        public Guid Id { get; set; }

        public Guid? DriverId { get; set; }
        public Guid? CustomerId { get; set; }
        [Required]
        public DateTime FromTime { get; set; }
        public DateTime? ToTime { get; set; }
        public DateTime? ScheduledTime { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string ClientPhoneNumber { get; set; }
        [Required]
        public string Description { get; set; }
        public bool IsScheduled { get; set; }
        public bool IsCompleted { get; set; }
        public bool IsCancelled { get; set; }
        public bool IsHappening { get; set; }
        [Required]
        public decimal OriginLatitude { get; set; }
        [Required]
        public decimal OriginLongitude { get; set; }
        [Required]
        public decimal DestinationLatitude { get; set; }
        [Required]
        public decimal DestinationLongitude { get; set; }

        public string TripToken { get; set; }
        public Time Time { get; set; }
        public Weather Weather { get; set; }
    }
    [Serializable]
    public partial class TripManagementDTO
    {
        public virtual Customer Customer { get; set; }
        public virtual Driver Driver { get; set; }
    }
}
