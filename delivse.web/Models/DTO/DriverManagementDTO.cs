﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public class DriverManagementDTO
    {
        [Required]
        public bool IsActive { get; set; }
    }
}
