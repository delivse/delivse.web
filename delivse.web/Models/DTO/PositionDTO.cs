﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public class PositionDTO
    {
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
    }
}
