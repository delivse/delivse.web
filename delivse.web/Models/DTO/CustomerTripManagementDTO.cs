﻿using delivse.web.Models.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public class CustomerTripManagementDTO
    {
        [Required]
        public Guid CustomerId { get; set; }
        [Required]
        public DateTime FromTime { get; set; }
        [Required]
        public DateTime? ToTime { get; set; }
        [Required]
        public DateTime? ScheduledTime { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string ClientPhoneNumber { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool IsScheduled { get; set; }
        [Required]
        public bool IsCompleted { get; set; }
        [Required]
        public bool IsCancelled { get; set; }
        [Required]
        public decimal OriginLatitude { get; set; }
        [Required]
        public decimal OriginLongitude { get; set; }
        [Required]
        public decimal DestinationLatitude { get; set; }
        [Required]
        public decimal DestinationLongitude { get; set; }
    }
}
