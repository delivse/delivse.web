﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models.DTO
{
    public partial class CustomerManagementDTO
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid? CustomerId { get; set; }
        [Required]
        public Guid? DriverId { get; set; }
        [Required]
        public Guid TripId { get; set; }
        [Required]
        public DateTime Time { get; set; }
        [Required]
        public short Ratings { get; set; }
        [Required]
        public string Comments { get; set; }
    }
}
