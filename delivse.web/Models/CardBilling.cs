﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    /// <summary>
    /// Card Credentials For User
    /// </summary>
    public partial class CardBilling
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public byte[] CardNumber { get; set; }
        public byte[] CVV { get; set; }
        public string ShortKey { get; set; }
        public string PostalCode { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string AccountName { get; set; }
        public Guid CustomerId { get; set; }
    }

    public partial class CardBilling
    {
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
    }
}
