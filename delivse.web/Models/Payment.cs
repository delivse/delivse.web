﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    public partial class Payment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; } 
        public decimal? Amount { get; set; }
        public bool IsPaid { get; set; }
        public Guid? CustomerId { get; set; }
        public Guid? DriverId { get; set; }
        public Guid? TripId { get; set; }
        public DateTime Date { get; }
    }

    [Serializable]
    public partial class Payment
    {
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        [ForeignKey("DriverId")]
        public virtual Driver Driver { get; set; }
        [JsonIgnore]
        [ForeignKey("TripId")]
        public virtual TripTracker TripTracker { get; set; }
    }
}
