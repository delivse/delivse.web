﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    /// <summary>
    /// Define the Driver Profile
    /// </summary>
    public partial class Driver
    {   
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string DriverId { get; set; }
        [StringLength(8)]
        public string GeneratedID { get; set; }
        public string FullName { get; set; }
        public short Ratings { get; set; }
        public string City { get; set; }
        public Enum.Gender Gender { get; set; }
        public string IDNumber { get; set; }
        public string LogbookNumber { get; set; }
        public string TaxPayerPIN { get; set; }
        public string VehicleDescription { get; set; }
    }
    [Serializable]
    public partial class Driver
    {
        [JsonIgnore]
        [ForeignKey("DriverId")]
        public virtual DelviseUser QuanDriver { get; set; }
        [JsonIgnore]
        public virtual ICollection<DriverManagement> DriverManagements { get; set; }
        [JsonIgnore]
        public virtual ICollection<DriverPositionManagement> DriverPositions { get; set; }
    }
}
