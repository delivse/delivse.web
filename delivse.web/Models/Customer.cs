﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    /// <summary>
    /// Define User Profile
    /// </summary>
    public partial class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string CustomerId { get; set; }
        public string FullName { get; set; }
        public string City { get; set; }
    }

    [Serializable]
    public partial class Customer
    {
        [JsonIgnore]
        [ForeignKey("CustomerId")]
        public virtual DelviseUser QuanUser { get; set; }
        [JsonIgnore]
        public virtual ICollection<CustomerManagement> Managements { get; set; }
        [JsonIgnore]
        public virtual ICollection<Payment> Payments { get; set; }
    }
}
