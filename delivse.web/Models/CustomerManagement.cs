﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{
    public partial class CustomerManagement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid? CustomerId { get; set; }
        public Guid? DriverId { get; set; }
        public Guid TripId { get; set; }
        public DateTime Time { get; set; }
        public short Ratings { get; set; }
        public string Comments { get; set; }

    }
    [Serializable]
    public partial class CustomerManagement
    {
        [ForeignKey("TripId")]
        public virtual TripManagement Trip { get; set; }
        [JsonIgnore]
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; private set; }
        [JsonIgnore]
        [ForeignKey("DriverId")]
        public virtual Driver Driver { get; set; }
    }
}
