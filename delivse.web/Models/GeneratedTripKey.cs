﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace delivse.web.Models
{

    public partial class GeneratedTripKey
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string ClientPhoneNumber { get; set; }
        [StringLength(10)]
        public string GeneratedCode { get; set; }
        public bool IsDelivered { get; set; }
        public Guid TripId { get; set; }
    }

    [Serializable]
    public partial class GeneratedTripKey
    {
        [ForeignKey("TripId")]
        public virtual TripManagement Trip { get; set; }
        [JsonIgnore]
        public virtual Driver Driver { get; set; }

    }
}
